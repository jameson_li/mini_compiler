tree grammar CheckSemantics;

options
{
   tokenVocab=Mini;
   ASTLabelType=CommonTree;
}

/*
   Tree Parser -- checks semantics of AST
*/
@header
{
   import javax.json.*;
   import java.util.LinkedHashMap;
}

@members
{

  private static final boolean DEBUG_MODE = true;

  private int numErrors = 0;
  private boolean validMainFunctionFound = false;

  public LinkedHashMap<String,Type> globalStructTable = new LinkedHashMap<String,Type>();

  public LinkedHashMap<String,Type> globalSymbolTable = new LinkedHashMap<String,Type>();
  private LinkedHashMap<String,Type> currentSymbolTable = null;

  private FunctionType curProcFunc = null;

  private String[] returnType = new String[2];
  private boolean functionReturned = false;
  private boolean hasErrors = false;

  private boolean isPrimitiveType(String type) {
    if(type.equals("int") ||
       type.equals("bool")) {
      return true;
    }

    return false;
  }

  private boolean isStructDefined(String type) {
    return globalStructTable.containsKey(type);
  }

  private boolean isDefined(String type) {
    boolean ret = false;

    if(currentSymbolTable != null) {
      ret = currentSymbolTable.containsKey(type);
    }

    return ret ? ret : globalSymbolTable.containsKey(type);
  }

  private boolean isDefinedFunction(String type) {
    return isDefined(type);
  }

  private Type getType(String type) {
    if(isDefined(type)) {
      Type ret = currentSymbolTable.get(type);

      return ret == null ? globalSymbolTable.get(type) : ret;
    }

    return null;
  }

  private String getTypeFromStruct(String structType, String structField) {
    StructType sType = (StructType) globalStructTable.get(structType);

    if(sType != null) {
      return sType.getField(structField);
    } else {
      return null;
    }
  }

  private boolean isTypeInStruct(String structType, String structField) {
    if(getTypeFromStruct(structType, structField) == null) {
      return false;
    }

    return true;
  }

  private String[] getParamTypes(String id) {
    FunctionType func = (FunctionType) globalSymbolTable.get(id);

    if(func != null) {
      return func.getParamsArray();
    }
    return null;
  }

  private boolean ifInvocationParamValid(String id, String pType, int paramCount) {
    String[] paramList = getParamTypes(id);

    if(paramList != null) {
      if(paramCount >= paramList.length) {
        return false;
      } else if((pType).equals(paramList[paramCount])) {
        return true;
      } else if(pType.equals("null") && isStructDefined(paramList[paramCount])) {
        return true;
      } else {
        return false;
      }
    }

    return false;
  }

  public boolean hasErrors() {
   if(!validMainFunctionFound) {
      dbg_print("Main function not found");
      return true;
   }

   return hasErrors;
  }

  public String getErrorMessage() {
    return "";
  }

  private void dbg_print(String str) {
    if(DEBUG_MODE == true) {
      hasErrors = true;
      System.err.println(str);
    }
  }

}

translate
   returns [int numErrors = 0]
   :  ^(PROGRAM t=types d=declarations[globalSymbolTable] f=functions)
         {

         }
   ;

types
   returns [int numErrors = 0]
   :  ^(TYPES (t=type_decl)*)
       { }
   |  { }
   ;

type_decl
   returns [int numErrors = 0]
   @init {StructType nStruct = new StructType();}
   :  ^(ast=STRUCT
       (id=ID) { nStruct.id = $id.text; } n=nested_decl[nStruct])
      {
        globalStructTable.put(nStruct.id, nStruct);
      }
   ;

nested_decl[StructType nStruct]
   returns [int numErrors = 0]
   :  (f=field_decl[nStruct] { })+
      { }
   ;

field_decl[StructType nStruct]
   returns [int numErrors = 0]
   :  ^(DECL ^(TYPE t=type) id=ID)
      {
         if(!isPrimitiveType($t.str) && !isStructDefined($t.str) && !(nStruct.id).equals($t.str)) {
            numErrors++;
            dbg_print("Found invalid type line: " + $id.line);
         } else {
            nStruct.fields.put($id.text, $t.str);
            nStruct.fieldHelper.add($id.text);
         }
      }
   ;

type
   returns [String str = null]
   :   INT { str = "int"; }
   |  BOOL { str = "bool"; }
   |  ^(STRUCT id=ID)
       {
          str = $id.text;
       }
   ;

declarations[LinkedHashMap<String,Type> list]
   returns [int numErrors = 0]
   :  ^(DECLS (d=decl_list[list])*)
      {  }
   |  {  }
   ;

decl_list[LinkedHashMap<String,Type> list]
   :  ^(DECLLIST ^(TYPE t=type)
         (id=ID
            {
              list.put($id.text, new Type($id.text, $t.str));
            }
         )+
      )
   ;

functions
   returns [int numErrors = 0]
   :  ^(FUNCS (f=function {  })*)
       { }
   |   { }
   ;

function
   returns [int numErrors = 0]
   @init
   {
     currentSymbolTable = new LinkedHashMap<String, Type>();
     FunctionType nFunc = new FunctionType();
   }
   :  ^(ast=FUN (id=ID {curProcFunc = nFunc; nFunc.id = $id.text; nFunc.type = "function"; globalSymbolTable.put(nFunc.id, nFunc);})
          p=parameters[nFunc] r=return_type[nFunc] d=declarations[currentSymbolTable] s=statement_list)
      {
         if(($id.text).equals("main")) {
            if(($r.str).equals("int") && $p.numParams == 0) {
               validMainFunctionFound = true;
            }
         }

         if($s.ret || (curProcFunc.ret_type).equals("void")) {
            //do nothing
         } else {
            dbg_print("Missing return statement or invalid return type in function: "
              + curProcFunc.id);
         }

         //save currentSymbolTable
         nFunc.localSymbolTable = currentSymbolTable;

         curProcFunc = null;
         currentSymbolTable = null;
      }
   ;

parameters[FunctionType nFunc]
   returns [int numParams = 0]
   :  ^(PARAMS (p=param_decl[nFunc] { numParams++; })*)
      {
        nFunc.numArgs = numParams;
      }
   ;

param_decl[FunctionType nFunc]
   returns [int numErrors = 0]
   :  ^(DECL ^(TYPE t=type) id=ID)
      {
        nFunc.argTypes.add($t.str);
        currentSymbolTable.put($id.text, new Type($id.text, $t.str));
      }
   ;

return_type[FunctionType nFunc]
   returns [String str = null]
   :  ^(RETTYPE rtype)
      {
        str = $rtype.str;
        nFunc.ret_type = $rtype.str;
      }
   ;

rtype
   returns [String str = null]
   :  t=type { str = $t.str; }
   |  VOID { str = "void"; }
   ;

statement
   returns [boolean ret = false]
   :  (b=block { ret = $b.ret; }
      |  s=assignment
      |  s=print
      |  s=read
      |  s=loop
      |  s=delete
      |  s=invocation_stmt
      )
      { }
   |  b=conditional { ret = $b.ret; }
   |  a=return_stmt
      {
        if($a.type != null) {
           if((curProcFunc.ret_type).equals($a.type)) {
              ret = true;
           }
           else if(isStructDefined(curProcFunc.ret_type) && ((curProcFunc.ret_type).equals($a.type) || ($a.type).equals("null"))) {
              ret = true;
           }
        }
      }
   ;

block
   returns [boolean ret = false]
   :  ^(BLOCK s=statement_list)
      {
        ret = $s.ret;
      }
   ;

statement_list
   returns [boolean ret = false]
   :  ^(STMTS (s=statement {  })*)
      {
        if($s.ret) {
          ret = true;
        }
      }
   ;

assignment
   returns [int numErrors = 0]
   :  ^(ast=ASSIGN e=expression l=lvalue)
      {
        if(($l.type).equals($e.type) ||
          (($e.type).equals("null") && !isPrimitiveType($l.type))) {
          //do nothing
        } else {
          dbg_print("Left and right of assignment is not compatible line: " + $ast.line + "left: " + $l.type + "right: " + $e.type);
        }
      }
   ;

print
   returns [int numErrors = 0]
   @init { boolean endl = false; }
   :  ^(ast=PRINT e=expression (ENDL { endl = true; })?)
      {
        if(!($e.type).equals("int")) {
          dbg_print("Found invalid print argument line: " + $ast.line);
        }
      }
   ;

read
   returns [int numErrors = 0]
   :  ^(ast=READ l=lvalue)
      {
        if(!($l.type).equals("int")) {
          dbg_print("Found invalid read argument line: " + $ast.line);
        }
      }
   ;

conditional
   returns [boolean ret = false]
   :  ^(ast=IF g=expression t=block (e=block)?)
      {
        if(!($g.type).equals("bool")) {
          dbg_print("Found invalid guard in conditional line: " + $ast.line);
        }

        if($t.ret && $e.ret) {
          ret = true;
        }
      }
   ;

loop
   returns [int numErrors = 0]
   :  ^(ast=WHILE e=expression b=block expression)
      {
        if(!($e.type).equals("bool")) {
          dbg_print("Found invalid guard in loop line: " + $ast.line);
        }
      }
   ;

delete
   returns [int numErrors = 0]
   :  ^(ast=DELETE e=expression)
      {
        if(!isStructDefined($e.type)) {
          dbg_print("Found invalid delete argument line: " + $ast.line);
        }
      }
   ;

return_stmt
   returns [String type = null]
   :  ^(ast=RETURN (e=expression)?)
      {
        if($e.type != null) {
          type = $e.type;
        }
      }
   ;

invocation_stmt
   returns [int numErrors = 0]
   @init { int paramCount = 0; }
   :  ^(INVOKE id=ID
          ^(ARGS (e=expression
          {
            if(!ifInvocationParamValid($id.text, $e.type, paramCount++)) {
              dbg_print("Found invalid parameter, " + paramCount +
                        ", in function call line: " + $id.line);
            }
          })*))
      {
         if(!isDefinedFunction($id.text) && !(curProcFunc.id).equals($id.text)) {
            dbg_print("Found invalid function invocation line: " + $id.line);
         }
      }
   ;

invocation_exp
   returns [String type = ""]
   @init { int paramCount = 0; }
   :  ^(INVOKE id=ID
          ^(ARGS (e=expression
          {
            if(!ifInvocationParamValid($id.text, $e.type, paramCount++)) {
              dbg_print("Found invalid parameter, " + paramCount +
                        ", in function call line: " + $id.line);
            }
          })*))
      {
         if(!isDefinedFunction($id.text) && !(curProcFunc.id).equals($id.text)) {
            dbg_print("Found invalid function invocation line: " + $id.line);
         } else if((curProcFunc.id).equals($id.text)) {
            $type = curProcFunc.ret_type;
         } else {
            Type nType = getType($id.text);
            if(nType != null) {
              $type = ((FunctionType)nType).ret_type;
            }
         }
      }
   ;

lvalue
   returns [String type = null]
   :  id=ID
      {
        $type = getType($id.text).type;
      }
   |  ^(ast=DOT l=lvalue id=ID)
      {
        $type = getTypeFromStruct($l.type, $id.text);
      }
   ;

expression
   returns [String type = ""]
   :  ^((ast=AND | ast=OR) lft=expression rht=expression)
      {
        if(($lft.type).equals("bool") && ($rht.type).equals("bool")) {
          $type = "bool";
        } else {
          dbg_print("Found invalid operands for boolean operators line: " + $ast.line);
        }
      }
   |  ^((ast=EQ | ast=NE) lft=expression rht=expression)
      {
        if(isStructDefined($lft.type) && (($lft.type).equals($rht.type) || ($rht.type).equals("null"))) {
          $type = "bool";
        } else if(($lft.type).equals("int") && ($rht.type).equals("int")) {
          $type = "bool";
        } else {
          dbg_print("Found invalid operands for equal operator line: " + $ast.line);
        }
      }
   |  ^((ast=LT | ast=GT | ast=LE | ast=GE) lft=expression rht=expression)
      {
        if(($lft.type).equals("int") && (($rht.type).equals("int") || ($rht.type).equals("null"))) {
          $type = "bool";
        } else {
          dbg_print("Found invalid operands for relational operators line: " + $ast.line);
        }
      }
   |  ^((ast=PLUS | ast=MINUS | ast=TIMES | ast=DIVIDE) lft=expression rht=expression)
      {
        if(!($lft.type).equals("int") || !($rht.type).equals("int")) {
          dbg_print("Found invalid operands for arthmetic line: " + $ast.line);
        } else {
          $type = "int";
        }
      }
   |  ^(ast=NOT e=expression)
      {
        if(($e.type).equals("bool")) {
          $type = "bool";
        } else {
          dbg_print("Found invalid operands for boolean operators line: " + $ast.line);
        }
      }
   |  ^(ast=NEG e=expression)
      {
        $type = "int";
      }
   |  ^(ast=DOT e=expression id=ID)
      {
        if(!isTypeInStruct($e.type, $id.text)) {
          dbg_print("Found use of undefined variable or field line: " + $ast.line);
        } else {
          $type = getTypeFromStruct($e.type, $id.text);
        }
      }
   |  e=invocation_exp
      {
        $type = $e.type;
      }
   |  id=ID
      {
        if(!isDefined($id.text)) {
          dbg_print("Found use of undefined variable line: " + $id.line);
        } else {
          $type = getType($id.text).type;
        }
      }
   |  i=INTEGER
      {
        $type = "int";
      }
   |  ast=TRUE
      {
        $type = "bool";
      }
   |  ast=FALSE
      {
        $type = "bool";
      }
   |  ^(ast=NEW id=ID)
      {
        if(isStructDefined($id.text)) {
          $type = $id.text; //struct type is the id
        } else {
          dbg_print("Found invalid use of new on a non-structure type line: " + $id.line);
        }
      }
   |  ast=NULL
      {
        $type = "null";
      }
   ;
