public class Type {
  String id;
  String type;
  int regNum;

  public Type() {}

  public Type(String id, String type) {
    this.id = id;
    this.type = type;
 }

 /* used for checking contains */
//FIXME:
 public Type(String id) {
    this.id = id;
 }

 @Override
 public boolean equals(Object obj) {
    if(obj == null) {
      return false;
   }
   if(!(obj instanceof Type)) {
      return false;
   }

   Type someType = (Type) obj;

   if((someType.id).equals(id)) {
      return true;
   }

   return false;
}
}