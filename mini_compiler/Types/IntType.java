
public class IntType extends Type {
  int value;

  public IntType(String id, String type) {
    super(id, "int");
  }

  public IntType(String value) {
    this.value = Integer.parseInt(value);
  }
}