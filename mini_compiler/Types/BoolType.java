public class BoolType extends Type {
  boolean value;

  public BoolType(String id, String type) {
    super(id, type);
  }
}