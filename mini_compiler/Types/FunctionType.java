import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;

import java.lang.StringBuilder;

import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;

public class FunctionType extends Type {
   String ret_type;
   int numArgs;
   String returnLabel;

   ArrayList<String> argTypes;
   LinkedHashMap<String,Type> localSymbolTable;

   BasicBlock basicBlock;

   Graph interGraph;
   ArrayList<String> unColoredNodes;
   ArrayList<InstructionX86> calleeSaved;
   // <Virtual Reg, Real Reg>
   HashMap<String,String> coloredNodes;

   RegisterAllocator regAlloc = RegisterAllocator.getInstance();

   public FunctionType() {
      argTypes = new ArrayList<String>();
      basicBlock = new BasicBlock();

      this.type = "function";
   }

   public FunctionType(String id, String type) {
      super(id, type);
      argTypes = new ArrayList<String>();
      basicBlock = new BasicBlock();
      this.type = "function";
   }

   public String[] getParamsArray() {
      return Arrays.copyOf(argTypes.toArray(), argTypes.size(), String[].class);
   }

   public void genAnalysis(HashMap<String,Type> globalSymbolTable) {
      if(regAlloc.functionInline) {
         basicBlock.functionInline(globalSymbolTable, false);
      }

      basicBlock.genIloc();

      if(regAlloc.copyProp) {
         basicBlock.copyPropagation(null);
      }
   }

   public void genX86_64Analysis() {
      basicBlock.genX86_64();

      // compute liveness
      while(!basicBlock.isLiveOutComplete()) {
         basicBlock.calculateLive(null);
      }

      if(regAlloc.uselessCode) {
         basicBlock.removeUselessCode();
      }
   }

   public String getX86_64() {
      StringBuilder builder = new StringBuilder();
      PreprocessorData pData = PreprocessorData.getInstance();

      // preprocessor
      pData.addFunctionHeader(id);

      builder.append(id + ":\n");

      //init inst. for function
      builder.append("\tpushq " + Register.RBP.getStr() + "\n");
      builder.append("\tmovq " + Register.RSP.getStr() + ", " + Register.RBP.getStr() + "\n");

      String retStr = builder.toString() + basicBlock.getX86_64() + "\n\n\n\n\n\n";
      retStr = pData.getString() + retStr;

      if(regAlloc.replaceMode) {
         int spillSize = reassignRegisters();
         String ret = rewriteInstructions(spillSize);
         pData.clearString();
         return ret;
      } else {
         pData.clearString();
         return retStr;
      }
   }

   public String rewriteInstructions(int spillSize) {
      StringBuilder builder = new StringBuilder();
      PreprocessorData pData = PreprocessorData.getInstance();

      builder.append(id + ":\n");

      //init inst. for function
      builder.append("\t" + X86Op.PUSHQ.getStr() + " " + Register.RBP.getStr() + "\n");
      builder.append("\t" + X86Op.MOVQ.getStr() + " " + Register.RSP.getStr() + ", " + Register.RBP.getStr() + "\n");
      builder.append("\t" + X86Op.ADDQ.getStr() + " " + "$-" + spillSize + ", " + Register.RSP.getStr() + "\n");

      RegisterAllocator.getInstance().spillSize = spillSize;

      for(InstructionX86 inst : calleeSaved) {
         builder.append(inst.toString());
      }

      basicBlock.replaceInstructions(coloredNodes);

      // swap lft and rht vals to restore callee saved register values
      StringBuilder calleeSavedBuilder = new StringBuilder();
      for(InstructionX86 inst : calleeSaved) {
         String tempLeft = inst.lftVal;

         inst.lftVal = inst.rhtVal;
         inst.rhtVal = tempLeft;

         calleeSavedBuilder.append(inst.toString());
      }

      StringBuilder retStr = new StringBuilder();
      // restore rsp, leave, ret
      retStr.append("\t" + X86Op.ADDQ.getStr() + " $" + spillSize + ", " + Register.RSP.getStr() + "\n");
      retStr.append("\t" + X86Op.LEAVE.getStr() + "\n");
      retStr.append("\t" + X86Op.RET.getStr() + "\n");

      return pData.getString() + builder.toString() + basicBlock.getX86_64()
         + calleeSavedBuilder.toString() + retStr.toString() + "\n\n\n\n\n";
   }

   @SuppressWarnings("unchecked")
   public int reassignRegisters() {
      interGraph = new SingleGraph("Function: " + id);
      unColoredNodes = new ArrayList<String>();
      coloredNodes = new HashMap<String,String>();
      calleeSaved = new ArrayList<InstructionX86>();

      List<Register> realRegList = Arrays.asList(
         Register.RBX, Register.RDI, Register.RSI, Register.RDX, Register.RCX,
         Register.RAX, Register.R8, Register.R9, Register.R11,
         Register.R12, Register.R13, Register.R14, Register.R15
         );
      int curColor = 0;

      // build interference graph
      interGraph.setAutoCreate(true);
      basicBlock.buildGraph(interGraph);

      // display graph
      //interGraph.display();

      // prepare for coloring
      for(Node n : interGraph) {
         if(Register.isRealReg(n.getId())) {
            coloredNodes.put(n.getId(), n.getId());
         } else {
            unColoredNodes.add(n.getId());
         }
      }

      int prevSize = -1;
      ArrayList<Register> calleeSavedRegisters = new ArrayList<Register> (
         Arrays.asList(Register.RBX, Register.R12, Register.R13, Register.R14,
         Register.R15));
      int spillSize = MiniUtils.DSIZE;

      // color graph
      while(unColoredNodes.size() > 0) {
         if(unColoredNodes.size() == prevSize) {
            break;
         }

         prevSize = unColoredNodes.size();
         curColor = 0;

         ArrayList<String> unColoredNodesDup = (ArrayList<String>) unColoredNodes.clone();

         for(Iterator<String> itera = unColoredNodesDup.iterator(); itera.hasNext() && curColor < realRegList.size();) {
            String n = itera.next();
            String iterColor = realRegList.get(curColor).getStr();
            Node nNode = interGraph.getNode(n);

            if(!findColorConflict(nNode, iterColor)) {
               coloredNodes.put(n, iterColor);
               unColoredNodes.remove(n);

               // save callee saved regsiters
               if(calleeSavedRegisters.contains(realRegList.get(curColor))) {
                  calleeSaved.add(new InstructionX86(X86Op.MOVQ, iterColor, spillSize+"(" + Register.RSP.getStr() + ")"));
                  calleeSavedRegisters.remove(realRegList.get(curColor));
                  spillSize += MiniUtils.DSIZE;
               }
            }

            ArrayList<String> unColoredNodesDup2 = (ArrayList<String>) unColoredNodes.clone();

            for(Iterator<String> iterb = unColoredNodesDup2.iterator(); iterb.hasNext();) {
               String m = iterb.next();
               Node mNode = interGraph.getNode(m);

               if((!m.equals(n)) && !mNode.hasEdgeBetween(n)) {

                  if(!findColorConflict(mNode, iterColor)) {
                     coloredNodes.put(m, iterColor);
                     unColoredNodes.remove(m);

                     // save callee saved regsiters
                     if(calleeSavedRegisters.contains(realRegList.get(curColor))) {
                        calleeSaved.add(new InstructionX86(X86Op.MOVQ, iterColor, spillSize+"(" + Register.RSP.getStr() + ")"));
                        calleeSavedRegisters.remove(realRegList.get(curColor));
                        spillSize += MiniUtils.DSIZE;
                     }
                  }
               }
            }
            curColor++;
         }
      }

      if(unColoredNodes.size() > 0) {
         for(Iterator<String> iter = unColoredNodes.iterator(); iter.hasNext();) {
            String cNode = iter.next();

            coloredNodes.put(cNode, spillSize+"(" + Register.RSP.getStr() + ")");
            iter.remove();

            spillSize += MiniUtils.DSIZE;
         }
      }

      return spillSize;
   }

   public boolean findColorConflict(Node mNode, String iterColor) {
      for(Iterator<Node> iterc = mNode.getNeighborNodeIterator(); iterc.hasNext();) {
         Node b = iterc.next();
         String l = b.getId();

         if(coloredNodes.containsKey(l) && ((coloredNodes.get(l)).equals(iterColor))) {
            return true;
         }
      }

      return false;
   }

   public String toString() {
      return id + ":\n" + basicBlock.toString() + "\n\n\n\n\n\n";
   }

   public boolean isInlineCandidate() {
      return basicBlock.getBlockSize() < 5;
   }
}
