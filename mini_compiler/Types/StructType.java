import java.util.LinkedHashMap;
import java.util.ArrayList;

public class StructType extends Type {
  /* id, type */
  LinkedHashMap<String, String> fields;

  ArrayList<String> fieldHelper;

  public StructType() {
    fields = new LinkedHashMap<String, String>();
    fieldHelper = new ArrayList<String>();
  }

  public StructType(String id, String type) {
    super(id, type);
    fields = new LinkedHashMap<String, String>();
    fieldHelper = new ArrayList<String>();
  }

  public String getField(String field) {
    return fields.get(field);
  }

  // expect @field
  public int getOffset(String str) {
     String field = str.split("@")[1];
     return fieldHelper.indexOf(field);
  }


}