public class CopyEntry {
   String dest;
   String src;
   Instruction ref;

   public CopyEntry(String dest, String src, Instruction ref) {
      this.dest = dest;
      this.src = src;
      this.ref = ref;
   }

   @Override
   public boolean equals(Object obj) {
      if(obj instanceof CopyEntry) {
         CopyEntry comparer = (CopyEntry) obj;
         return dest.equals(comparer.dest);
      }

      return false;
   }

}
