import os
import sys
import time

from compiler_args import cargs

path = "tests/benchmarks"
minArgs = cargs
#minArgs = "-s -r -dumpIL"
#minArgs = "-s -r -dumpIL -fi"
#minArgs = "-ucr -s -r -dumpIL"
#minArgs = "-cp -ucr -s -r -dumpIL"
CP = "-cp .:lib/gs-core-1.3.jar:lib/antlr-3.3-complete.jar:lib/javax.json-1.0.4.jar"
numpass = 0
totaltestruns = 0

def runTest(testName):
   global totaltestruns
   global numpass
   print "------------Testing " + testName + "----------------"
   totaltestruns = totaltestruns + 1
   tempOut = "_temp_.out"
   tempDiff = "_tempDiff_.out"
   pParent = path + "/" + testName
   p = path + "/" + testName + "/" + testName
   ret = os.system("java " + CP + " Mini " + minArgs + " " + p + ".mini")
   if(ret == 0):
      ret = os.system("gcc -g -o " + p + ".o -c " + p + ".s")
   else:
      return
   if(ret == 0):
      ret = os.system("gcc -o " + p + ".out " + p + ".o")
   else:
      return
   if(ret == 0):
      ret = os.system("rm " + p + ".o")
   else:
      return
   if(ret == 0):
      os.system(p + ".out" + " <" + pParent + "/input" + ">" + tempOut);
      print "\tcomparing output for " + testName
      ret = os.system("diff -w -B " + tempOut + " " + pParent + "/output >" + tempDiff);
      if(ret == 0):
         numpass = numpass + 1
         print "\t\tmatch SUCCESS\n"
      else:
         print "\t\tmatch FAIL\n"
   else:
      return
   if os.path.isfile(tempOut):
      ret = os.system("rm " + tempOut);
   if os.path.isfile(tempDiff):
      ret = os.system("rm " + tempDiff);

   return;

print "---------------Building------------------"
ret = os.system("make");
if(ret != 0):
   exit(); 
if len(sys.argv) > 1:
   runTest(sys.argv[1]);
else:
   for root, subFolders, files in os.walk(path):
      for sub in subFolders:
         runTest(sub)
   print "\nTotal Test Runs: " + str(totaltestruns) + "/20"
   print "Test(s) Passed: " + str(numpass) + "/" + str(totaltestruns)
