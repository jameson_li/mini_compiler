import java.util.Queue;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.ArrayList;

import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;

public class LoopBlock extends BasicElement{

   public String guardLabel;

   public Expression guard;
   public LinkedList<Instruction> skipJumpStatements;
   public BasicBlock block;
   public LinkedList<Instruction> returnJumpStatements;

   public String afterLabel;

   public LoopBlock() {
      super();
      skipJumpStatements = new LinkedList<Instruction>();
      returnJumpStatements = new LinkedList<Instruction>();
   }

   public BasicElement calculateLive(BasicElement pred) {
      while(!isLiveOutComplete()) {
         oldLiveout = liveout;
         pred = calculateLiveH(pred);
      }

      return this;
   }

   public BasicElement calculateLiveH(BasicElement pred) {
      block.calculateLive(pred);
      ArrayList<String> blockLiveout = MiniUtils.calculateLiveOut(block);

      guard.calculateLive(block);
      ArrayList<String> guardLiveout = MiniUtils.calculateLiveOut(guard);

      liveout = MiniUtils.union(blockLiveout, guardLiveout);

      block.calculateLive(guard);
      ArrayList<String> blockGuardLiveout = MiniUtils.calculateLiveOut(block);
      liveout = MiniUtils.union(liveout, blockGuardLiveout);

      return this;
   }

   public void buildGraph(Graph graph) {
      guard.buildGraph(graph);
      block.buildGraph(graph);
   }

   public void replaceInstructions(HashMap<String,String> coloredNodes) {
      guard.replaceInstructions(coloredNodes);
      block.replaceInstructions(coloredNodes);
   }

   public void removeUselessCode() {
      guard.removeUselessCode();
      block.removeUselessCode();
   }

   public BasicElement copyPropagation(BasicElement pred) {
      guard.copyPropagation(pred);
      copyIn = MiniUtils.union(copyIn, guard.copyIn);
      block.copyPropagation(guard);
      copyIn = MiniUtils.union(copyIn,block.copyIn);

      return this;
   }

   public String getX86_64() {
      StringBuilder builder = new StringBuilder();

      builder.append(guardLabel + ":\n");
      builder.append(guard.getX86_64());

      for(Instruction i : skipJumpStatements) {
         builder.append(i.getX86_64());
      }

      builder.append(block.getX86_64());

      for(Instruction i : returnJumpStatements) {
         builder.append(i.getX86_64());
      }

      builder.append(afterLabel + ":\n");

      return builder.toString();
   }

   public void genIloc() {
      guard.genIloc();

      copyInGen = MiniUtils.union(copyInGen, guard.copyInGen);
      copyInKill = MiniUtils.union(copyInKill, guard.copyInKill);

      block.genIloc();

      copyInGen = MiniUtils.union(copyInGen, block.copyInGen);
      copyInKill = MiniUtils.union(copyInKill, block.copyInKill);
   }

   public void genX86_64() {
      guard.genX86_64();
      gen = MiniUtils.union(gen, guard.gen);
      kill = MiniUtils.union(kill, guard.kill);

      for(Instruction i : skipJumpStatements) {
         i.genX86_64();
      }

      block.genX86_64();
      gen = MiniUtils.union(gen, block.gen);
      kill = MiniUtils.union(kill, block.kill);

      for(Instruction i : returnJumpStatements) {
         i.genX86_64();
      }
   }

   public void functionInline(HashMap<String,Type> globalSymbolTable, boolean inline) {
      guard.functionInline(globalSymbolTable, true);
      block.functionInline(globalSymbolTable, true);
   }

   public void rewriteVR(String prefix) {
      guard.rewriteVR(prefix);
      block.rewriteVR(prefix);

      for(Instruction inst : skipJumpStatements) {
         inst.rewriteVR(prefix);
      }

      for(Instruction inst : returnJumpStatements) {
         inst.rewriteVR(prefix);
      }
   }

   public BasicElement duplicate() {
      LoopBlock dup = new LoopBlock();

      dup.label = label;
      dup.guardLabel = guardLabel;
      dup.afterLabel = afterLabel;

      dup.guard = (Expression) guard.duplicate();
      dup.block = (BasicBlock) block.duplicate();

      for(Instruction inst : skipJumpStatements) {
         Instruction newInst = inst.duplicate();
         dup.skipJumpStatements.add(newInst);
      }

      for(Instruction inst : returnJumpStatements) {
         Instruction newInst = inst.duplicate();
         dup.returnJumpStatements.add(newInst);
      }

      return dup;
   }

   public String toString() {
      StringBuilder builder = new StringBuilder();

      builder.append(guardLabel + ":\n");
      builder.append(guard.toString());

      for(Instruction i : skipJumpStatements) {
         builder.append(i.toString());
      }

      builder.append(block.toString());

      for(Instruction i : returnJumpStatements) {
         builder.append(i.toString());
      }

      builder.append(afterLabel + ":\n");

      return builder.toString();
   }

}
