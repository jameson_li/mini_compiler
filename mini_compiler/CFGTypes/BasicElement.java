import java.util.ArrayList;
import java.util.HashMap;

import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;

public class BasicElement {

   public String label;

   public boolean isReturnStatement = false;

   public ArrayList<String> gen = new ArrayList<String>();
   public ArrayList<String> kill = new ArrayList<String>();

   public ArrayList<String> oldLiveout = null;
   public ArrayList<String> liveout = null;
   public ArrayList<CopyEntry> copyIn = null;

   public ArrayList<CopyEntry> copyInGen = new ArrayList<CopyEntry>();
   public ArrayList<CopyEntry> copyInKill = new ArrayList<CopyEntry>();

   public BasicElement() {
      this.label = "no label assigned";
   }

   public BasicElement(String label) {
      this.label = label;
   }

   public String getX86_64() {
      MiniUtils.error("getX86_64 not implemented");
      return null;
   }

   public void genIloc() {
      MiniUtils.error("genIloc not implemented");
   }

   public void genX86_64() {
      MiniUtils.error("genX86_64 not implemented");
   }

   public void functionInline(HashMap<String,Type> globalSymbolTable, boolean inline) {
      MiniUtils.error("functionInline not implemented");
   }

   public BasicElement calculateLive(BasicElement pred) {
      MiniUtils.error("calculate live not implemented");
      return null;
   }

   public boolean isLiveOutComplete() {
      if(liveout != null && oldLiveout != null && liveout.size() == oldLiveout.size()) {
         for(String str : liveout) {
            if(!oldLiveout.contains(str)) {
               return false;
            }
         }
      } else {
         return false;
      }

      return true;
   }

   public void buildGraph(Graph graph) {
      // liveout build graph
      for(int i=0; i<liveout.size(); i++) {
         String cur = liveout.get(i);

         if(graph.getNode(cur) == null) {
            graph.addNode(cur);
         }

         for(int j=i+1; j<liveout.size(); j++) {
            String nNode = liveout.get(j);
            if(graph.getNode(nNode) == null) {
               graph.addNode(nNode);
            }

            if(graph.getEdge(cur+nNode) == null && graph.getEdge(nNode+cur) == null) {
               graph.addEdge(cur+nNode, cur, nNode);
            }
         }
      }
   }

   public void replaceInstructions(HashMap<String,String> coloredNodes) {}

   public void removeUselessCode() { MiniUtils.error("removeUselessCode not implemented"); }

   public BasicElement copyPropagation(BasicElement pred) {
      MiniUtils.error("removeUselessCode not implemented");
      return null;
   }

   public void replaceIlocRegisters() {
      MiniUtils.error("replaceIlocRegisters not implemented");
   }

   public int getBlockSize() {
      MiniUtils.error("getBlockSize not implemented");
      return 0;
   }

   public void rewriteVR(String prefix) {
      MiniUtils.error("rewriteVR not implemented");
   }

   public boolean isReturnStatement() {
      return isReturnStatement;
   }

   public BasicElement duplicate() {
      MiniUtils.error("duplicate not implemented");
      return null;
   }
}
