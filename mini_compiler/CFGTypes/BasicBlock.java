import java.util.LinkedList;
import java.util.Queue;
import java.util.Collections;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;

import java.lang.StringBuilder;

import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;

public class BasicBlock extends BasicElement {

   public LinkedList<BasicElement> statementList;

   public String afterLabel;

   private boolean isLiveOutComplete;

   public BasicBlock() {
      statementList = new LinkedList<BasicElement>();
   }

   public BasicBlock(String label) {
      super(label);
      statementList = new LinkedList<BasicElement>();
   }

   public void addStatement(BasicElement elem) {
      statementList.add(elem);
   }

   @SuppressWarnings("unchecked")
   public BasicElement calculateLive(BasicElement pred) {
      LinkedList<BasicElement> instListDup = (LinkedList<BasicElement>) statementList.clone();
      Collections.reverse(instListDup);

      oldLiveout = liveout;

      liveout = null;

      for(BasicElement inst : instListDup) {
         pred = inst.calculateLive(pred);

         liveout = (ArrayList<String>) MiniUtils.union(liveout, pred.liveout);
      }

      return this;
   }

   public void buildGraph(Graph graph) {
      for(BasicElement inst : statementList) {
         inst.buildGraph(graph);
      }
   }

   public String getX86_64() {
      StringBuilder builder = new StringBuilder();

      if(label != null) {
         builder.append(label + ":\n");
      }

      for(BasicElement elem : statementList) {
         if(elem != null) {
            builder.append(elem.getX86_64());
         }
      }

      if(afterLabel != null) {
         builder.append(afterLabel + ":\n");
      }

      return builder.toString();
   }

   public void genIloc() {
      for(BasicElement elem : statementList) {
         if(elem != null) {
            elem.genIloc();

            copyInGen = (ArrayList<CopyEntry>) MiniUtils.union(copyInGen, elem.copyInGen);
            copyInKill = (ArrayList<CopyEntry>) MiniUtils.union(copyInKill, elem.copyInKill);
         }
      }
   }

   public void genX86_64() {
      for(BasicElement elem : statementList) {
         if(elem != null) {
            elem.genX86_64();

            gen = (ArrayList<String>) MiniUtils.union(gen, elem.gen);
            kill = (ArrayList<String>) MiniUtils.union(kill, elem.kill);
         }
      }
   }

   @SuppressWarnings("unchecked")
   public void functionInline(HashMap<String,Type> globalSymbolTable, boolean inline) {
      if(inline) {
         LinkedList<BasicElement> dupStatementList = (LinkedList<BasicElement>) statementList.clone();
         for(Iterator<BasicElement> iter = dupStatementList.iterator(); iter.hasNext();) {
            BasicElement elem = iter.next();

            if(elem != null && (elem instanceof InstructionContainer)) {
               InstructionContainer ic = (InstructionContainer) elem;
               FunctionType funct = ic.getCalledFunction(globalSymbolTable);

               if(funct != null) {
                  // split ic, then insert funct
                  InstructionContainer storeArgsInst = ic.getStoreArgsInst();
                  InstructionContainer retLoadInst = ic.getRetStoreInst();

                  statementList.add(statementList.indexOf(elem), storeArgsInst);
                  statementList.add(statementList.indexOf(elem), funct.basicBlock.getInlineBlock());
                  statementList.add(statementList.indexOf(elem), retLoadInst);
                  statementList.remove(elem);

                  System.out.println("Split and insert should have happened");
               }
            }
         }
      } else {
         for(BasicElement inst : statementList) {
            inst.functionInline(globalSymbolTable, inline);
         }
      }
   }

   public void replaceInstructions(HashMap<String,String> coloredNodes) {
      for(BasicElement elem : statementList) {
         elem.replaceInstructions(coloredNodes);
      }
   }

   public void removeUselessCode() {
      for(BasicElement elem : statementList) {
         elem.removeUselessCode();
      }
   }

   public BasicElement copyPropagation(BasicElement pred) {
      for(BasicElement elem : statementList) {
         pred = elem.copyPropagation(pred);

         copyIn = (ArrayList<CopyEntry>) MiniUtils.union(copyIn, elem.copyIn);
      }

      return this;
   }

   public int getBlockSize() {
      return statementList.size();
   }

   public BasicBlock getInlineBlock() {
      BasicBlock newBlock = new BasicBlock(label+label);

      // rewrite Virtual regs
      for(BasicElement elem : statementList) {
         if(!elem.isReturnStatement()) {
            BasicElement dupElem = elem.duplicate();
            dupElem.rewriteVR(label);
            newBlock.statementList.add(dupElem);
         } else {
            // return statement
            newBlock.afterLabel = label+elem.label;
            break; // this won't work if the function has more than one return statement
         }
      }

      return newBlock;
   }

   public boolean isReturnStatement() {
      for(BasicElement elem : statementList) {
         if(elem.isReturnStatement) {
            return true;
         }
      }

      return false;
   }

   public void rewriteVR(String prefix) {
      for(BasicElement elem : statementList) {
         elem.rewriteVR(prefix);
      }

      if(label != null) {
         label = prefix+label;
      }
      if(afterLabel != null) {
         afterLabel = prefix+afterLabel;
      }
   }

   @SuppressWarnings("unchecked")
   public BasicElement duplicate() {
      BasicBlock dup = new BasicBlock(label);

      dup.label = label;

      for(BasicElement elem : statementList) {
         BasicElement newElem = elem.duplicate();
         dup.statementList.add(newElem);
      }

      dup.afterLabel = afterLabel;
      dup.isLiveOutComplete = isLiveOutComplete;

      return dup;
   }

   public String toString() {
      StringBuilder builder = new StringBuilder();

      if(label != null) {
         builder.append(label + ":\n");
      }

      for(BasicElement elem : statementList) {
         if(elem != null) {
            builder.append(elem.toString());
         }
      }

      if(afterLabel != null) {
         builder.append(afterLabel + ":\n");
      }

      return builder.toString();
   }
}
