
public class Expression extends InstructionContainer {

   String tempOpCode;

   boolean isDot = false;
   String fieldId = null;

   String globalId = null;

   public Expression() {
      super();
   }

   public BasicElement duplicate() {
      Expression dup = new Expression();

      dup.label = label;
      dup.type = type;
      dup.tempOpCode = tempOpCode;

      for(Instruction inst : instruct) {
         Instruction newInst = inst.duplicate();
         dup.instruct.add(newInst);
      }

      return dup;
   }
}
