import java.util.Queue;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.HashMap;

import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;

public class ConditionalBlock extends BasicElement{

   public Expression guard;
   public BasicBlock thenBlock;
   public BasicBlock elseBlock;

   public String afterLabel;

   public ConditionalBlock() {
      guard = new Expression();
   }

   public BasicElement calculateLive(BasicElement pred) {
      thenBlock.calculateLive(pred);

      oldLiveout = liveout;

      ArrayList<String> thenLiveout = MiniUtils.calculateLiveOut(thenBlock);
      liveout = thenLiveout;

      if(elseBlock != null) {
         elseBlock.calculateLive(pred);
         ArrayList<String> elseLiveout = MiniUtils.calculateLiveOut(elseBlock);

         liveout = MiniUtils.union(thenLiveout, elseLiveout);
      }

      guard.calculateLive(this);
      ArrayList<String> guardLiveout = MiniUtils.calculateLiveOut(guard);

      liveout = MiniUtils.union(liveout, guardLiveout);

      return this;
   }

   public void buildGraph(Graph graph) {
      guard.buildGraph(graph);
      thenBlock.buildGraph(graph);

      if(elseBlock != null) {
         elseBlock.buildGraph(graph);
      }
   }

   public void replaceInstructions(HashMap<String,String> coloredNodes) {
      guard.replaceInstructions(coloredNodes);
      thenBlock.replaceInstructions(coloredNodes);

      if(elseBlock != null) {
         elseBlock.replaceInstructions(coloredNodes);
      }
   }

   public void removeUselessCode() {
      guard.removeUselessCode();
      thenBlock.removeUselessCode();

      if(elseBlock != null) {
         elseBlock.removeUselessCode();
      }
   }

   public BasicElement copyPropagation(BasicElement pred) {
      guard.copyPropagation(pred);
      thenBlock.copyPropagation(guard);

      ArrayList<CopyEntry> guardCopyIn = MiniUtils.calculateCopyPropagation(guard);
      ArrayList<CopyEntry> thenCopyIn = MiniUtils.calculateCopyPropagation(thenBlock);
      copyIn = (ArrayList<CopyEntry>) MiniUtils.intersection(guardCopyIn, thenCopyIn);

      ArrayList<CopyEntry> elseCopyIn = new ArrayList<CopyEntry>();

      if(elseBlock != null) {
         elseBlock.copyPropagation(guard);
         elseCopyIn = MiniUtils.calculateCopyPropagation(elseBlock);
         copyIn = (ArrayList<CopyEntry>) MiniUtils.intersection(copyIn, elseCopyIn);
      }

      return this;
   }

   public String getX86_64() {
      StringBuilder builder = new StringBuilder();

      builder.append(guard.getX86_64());
      builder.append(thenBlock.getX86_64());

      if(elseBlock != null) {
         builder.append(elseBlock.getX86_64());
      }

      if(afterLabel != null) {
         builder.append(afterLabel + ":\n");
      }

      return builder.toString();
   }

   public void functionInline(HashMap<String,Type> globalSymbolTable, boolean inline) {
      if(inline) {
         guard.functionInline(globalSymbolTable, inline);
         thenBlock.functionInline(globalSymbolTable, inline);

         if(elseBlock != null) {
            elseBlock.functionInline(globalSymbolTable, inline);
         }
      }
   }

   public void genIloc() {
      guard.genIloc();
      thenBlock.genIloc();

      if(elseBlock != null) {
         elseBlock.genIloc();
      }
   }

   public void genX86_64() {
      guard.genX86_64();
      thenBlock.genX86_64();

      if(elseBlock != null) {
         elseBlock.genX86_64();
      }
   }

   public void rewriteVR(String prefix) {
      guard.rewriteVR(prefix);
      thenBlock.rewriteVR(prefix);

      if(elseBlock != null) {
         elseBlock.rewriteVR(prefix);
      }

      if(label != null) {
         label = prefix+label;
      }

      if(afterLabel != null) {
         afterLabel = prefix+afterLabel;
      }
   }

   public BasicElement duplicate() {
      ConditionalBlock dup = new ConditionalBlock();

      dup.label = label;
      dup.afterLabel = afterLabel;
      dup.guard = (Expression) guard.duplicate();
      dup.thenBlock = (BasicBlock) thenBlock.duplicate();

      if(elseBlock != null) {
         dup.elseBlock = (BasicBlock) elseBlock.duplicate();
      }

      return dup;
   }

   public String toString() {
      StringBuilder builder = new StringBuilder();

      builder.append(guard.toString());
      builder.append(thenBlock.toString());

      if(elseBlock != null) {
         builder.append(elseBlock.toString());
      }

      if(afterLabel != null) {
         builder.append(afterLabel + ":\n");
      }

      return builder.toString();
   }

}
