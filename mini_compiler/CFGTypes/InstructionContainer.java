import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Collections;

import java.lang.StringBuilder;

import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;

public class InstructionContainer extends BasicElement {

   public LinkedList<Instruction> instruct;
   public ArrayList<InstructionX86> instList;

   public String type;

   public String tempOpCode;

   public InstructionContainer() {
      instruct = new LinkedList<Instruction>();
      instList = new ArrayList<InstructionX86>();
   }

   @SuppressWarnings("unchecked")
   public BasicElement calculateLive(BasicElement pred) {

      if(instList.size() > 0) {
         oldLiveout = liveout; // save old list

         // calculating live now
         ArrayList<InstructionX86> instListRev = (ArrayList<InstructionX86>) instList.clone();
         Collections.reverse(instListRev);

         for(InstructionX86 inst : instListRev) {
            pred = inst.calculateLiveNow(pred);
            liveout = (ArrayList<String>) MiniUtils.union(liveout, pred.liveout);
         }

         return this;
      } else {
         if(pred != null) {
            oldLiveout = liveout = pred.liveout;
         } else {
            oldLiveout = liveout = new ArrayList<String>();
         }
         return this;
      }
   }

   public void buildGraph(Graph graph) {
      super.buildGraph(graph);

      // livenow build graph
      for(InstructionX86 inst: instList) {
         inst.buildGraph(graph);
      }
   }

   public String getX86_64() {
      StringBuilder builder = new StringBuilder();
      RegisterAllocator regAlloc = RegisterAllocator.getInstance();

      for (Instruction aInstruct : instruct) {
         builder.append(aInstruct.getX86_64());
      }

      // builder.append("Instruct Container: " + MiniUtils.dumpArrayList(liveout) + "\n");

      return builder.toString();
   }


   public void genIloc() {
      RegisterAllocator regAlloc = RegisterAllocator.getInstance();

      ArrayList<ArrayList<CopyEntry>> copyPropResult = regAlloc.copyPropagationAnalysis(instruct);
      copyInGen = copyPropResult.get(0);
      copyInKill = copyPropResult.get(1);
   }

   public void genX86_64() {
      RegisterAllocator regAlloc = RegisterAllocator.getInstance();

      for (Instruction aInstruct : instruct) {
         aInstruct.genX86_64();
         instList.addAll(aInstruct.instList);
      }

      ArrayList<ArrayList<String>> analysisResult = regAlloc.liveAnalysis(instList);
      gen = analysisResult.get(0);
      kill = analysisResult.get(1);
   }

   public void replaceInstructions(HashMap<String,String> coloredNodes) {
      for(Instruction inst : instruct) {
         inst.replaceInstructions(coloredNodes);
      }
   }

   public void removeUselessCode() {
      for(Instruction inst : instruct) {
         inst.removeUselessCode();
      }
   }

   public BasicElement copyPropagation(BasicElement pred) {
      copyIn = MiniUtils.calculateCopyPropagation(pred);

      for(Instruction inst : instruct) {
         // replace instruction here using copyIn
         inst.rewriteRegisters(copyIn);
      }

      return this;
   }

   public void functionInline(HashMap<String,Type> globalSymbolTable, boolean inline) {
      return;
   }

   @SuppressWarnings("unchecked")
   public FunctionType getCalledFunction(HashMap<String,Type> globalSymbolTable) {
      for(Instruction inst : instruct) {
         if(inst.op.equals("call")) {
            String functName = inst.lftVal;
            Type funct = globalSymbolTable.get(functName);

            if(funct instanceof FunctionType) {
               FunctionType functType = (FunctionType) funct;
               if(functType.isInlineCandidate()) {
                  return functType;
               }

            } else {
               System.out.println("attempt to find: " + functName);
            }
         }
      }

      return null;
   }

   public InstructionContainer getStoreArgsInst() {
      InstructionContainer newInst = new InstructionContainer();
      LinkedList<Instruction> newInstruct = new LinkedList<Instruction>();

      newInst.label = label;
      newInst.instruct = newInstruct;

      for(Instruction inst : instruct) {
         if(inst.op.equals("call")) {
            break;
         }
         newInstruct.addLast(inst);
      }

      return newInst;
   }

   @SuppressWarnings("unchecked")
   public InstructionContainer getRetStoreInst() {
      InstructionContainer newInst = new InstructionContainer();
      LinkedList<Instruction> newInstruct = new LinkedList<Instruction>();

      newInst.label = label;
      newInst.instruct = newInstruct;

      LinkedList<Instruction> dupInstructRev = (LinkedList<Instruction>) instruct.clone();
      Collections.reverse(dupInstructRev);

      for(Instruction inst : dupInstructRev) {
         if(inst.op.equals("call")) {
            break;
         } else if(inst.op.equals("loadret")) {
            inst.midVal = "bypass";
         }
         newInstruct.addFirst(inst);
      }

      return newInst;
   }

   public void rewriteVR(String prefix) {
      for(Instruction inst : instruct) {
         inst.rewriteVR(prefix);
      }
   }

   public BasicElement duplicate() {
      InstructionContainer dup = new InstructionContainer();

      dup.label = label;
      dup.type = type;
      dup.tempOpCode = tempOpCode;

      for(Instruction inst : instruct) {
         Instruction newInst = inst.duplicate();
         dup.instruct.add(newInst);
      }

      return dup;
   }

   public String toString() {
      StringBuilder builder = new StringBuilder();

      for (Instruction aInstruct : instruct) {
         builder.append(aInstruct.toString());
      }

      return builder.toString();
   }
}
