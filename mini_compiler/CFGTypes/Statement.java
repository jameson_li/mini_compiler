import java.util.LinkedList;
import java.lang.StringBuilder;

public class Statement extends InstructionContainer {

   public Statement() {
      super();
   }

   public BasicElement duplicate() {
      Statement dup = new Statement();

      dup.label = label;
      dup.type = type;
      dup.tempOpCode = tempOpCode;

      for(Instruction inst : instruct) {
         Instruction newInst = inst.duplicate();
         dup.instruct.add(newInst);
      }

      return dup;
   }
}
