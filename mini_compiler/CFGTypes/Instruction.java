import java.lang.StringBuilder;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;

public class Instruction {

   public String op;

   public String lftVal; //left register or immediate value
   public String midVal; //right register
   public String rhtVal; //destination register or source register

   public String parentType;
   public boolean isDot = false; // use with read instruction

   private RegisterAllocator regAlloc = RegisterAllocator.getInstance();

   public ArrayList<InstructionX86> instList;

   public Instruction(String op, String rhtVal) {
      this.op = op;
      this.rhtVal = rhtVal;
   }

   public Instruction(String op, String lftVal, String rhtVal) {
      this.op = op;
      this.lftVal = lftVal;
      this.rhtVal = rhtVal;
   }

   public Instruction(String op, String lftVal, String midVal, String rhtVal) {
      this.op = op;
      this.lftVal = lftVal;
      this.midVal = midVal;
      this.rhtVal = rhtVal;
   }

   @SuppressWarnings("unchecked")
   public void replaceInstructions(HashMap<String,String> coloredNodes) {
      ArrayList<InstructionX86> dupList = (ArrayList<InstructionX86>)instList.clone();
      for(Iterator<InstructionX86> iter = dupList.iterator(); iter.hasNext();) {
         InstructionX86 inst = iter.next();
         inst.replaceInstructionsX86(coloredNodes, instList);
      }
   }

   @SuppressWarnings("unchecked")
   public void removeUselessCode() {
      ArrayList<InstructionX86> dupList = (ArrayList<InstructionX86>)instList.clone();
      for(Iterator<InstructionX86> iter = dupList.iterator(); iter.hasNext();) {
         InstructionX86 inst = iter.next();
         inst.removeUselessCodeX86(instList);
      }
   }

   public String instListToString() {
      StringBuilder builder = new StringBuilder();

      for(InstructionX86 elem : instList) {
         builder.append(elem.toString());
      }

      return builder.toString();
   }

   public String getX86_64() {
      StringBuilder builder = new StringBuilder();

      builder.append(instListToString());

      return builder.toString();
   }

   public void rewriteVR(String prefix) {
      lftVal = rewriteVRH(lftVal, prefix);
      midVal = rewriteVRH(midVal, prefix);
      rhtVal = rewriteVRH(rhtVal, prefix);
   }

   public String rewriteVRH(String replaced, String prefix) {
      if(replaced != null && (replaced.matches("r[0-9]+") || replaced.matches("L[0-9]+"))) {
         return prefix + replaced;
      }

      return replaced;
   }

   public void genX86_64() {
      PreprocessorData pData = PreprocessorData.getInstance();
      String temp;

      if(instList != null) {
         return;
      }

      instList = new ArrayList<InstructionX86>();

      switch(op) {
/*****Load/Store******/
         case "loadinargument":
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.getArgRegister(Integer.parseInt(midVal)), regAlloc.assign(rhtVal)));
            break;
         case "storeoutargument":
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(lftVal), regAlloc.getArgRegister(Integer.parseInt(rhtVal))));
            break;
         case "storeglobal":
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(lftVal), rhtVal + "(" + Register.RIP.getStr() + ")"));
            break;
         case "loadglobal":
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(lftVal) + "(" + Register.RIP.getStr() + ")", rhtVal));
            break;
         case "mov":
            instList.add(new InstructionX86(X86Op.MOVQ, lftVal, rhtVal));
            break;
         case "loadi":
            instList.add(new InstructionX86(X86Op.MOVQ, "$" + lftVal, regAlloc.assign(rhtVal)));
            break;
         case "loadai":
            int lOffset = MiniUtils.DSIZE * ((StructType)Mini.globalStructTable.get(parentType)).getOffset(midVal);
            instList.add(new InstructionX86(X86Op.MOVQ, lOffset + "(" + regAlloc.assign(lftVal) + ")", regAlloc.assign(rhtVal)));
            break;
         case "loadaia":
            lOffset = MiniUtils.DSIZE * ((StructType)Mini.globalStructTable.get(parentType)).getOffset(midVal);
            instList.add(new InstructionX86(X86Op.LEA, lOffset + "(" + regAlloc.assign(lftVal) + ")", regAlloc.assign(rhtVal)));
            break;
         case "storeai":
            //int sOffset = MiniUtils.DSIZE * ((StructType)Mini.globalStructTable.get(parentType)).getOffset(rhtVal);
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(lftVal), "0(" + regAlloc.assign(midVal) + ")"));
            break;

/*****Address Computation******/
         case "computeformaladdress":
            break;
         case "computeglobaladdress": // this is likely not used
            break;

/*****Read/Print******/
         case "print":
            pData.insertPrintHeader(false);
            instList.add(new InstructionX86(X86Op.MOVQ, "$.LLC0", regAlloc.getArgRegister(0)));
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(rhtVal), regAlloc.getArgRegister(1)));
            pushCallerSaved();
            instList.add(new InstructionX86(X86Op.MOVQ, "$0", Register.RAX.getStr()));
            InstructionX86 printCallInst = new InstructionX86(X86Op.CALL, "printf");
            printCallInst.numArgs = 2;
            instList.add(printCallInst);
            popCallerSaved();
            break;
         case "println":
            pData.insertPrintHeader(true);
            instList.add(new InstructionX86(X86Op.MOVQ, "$.LLC1", regAlloc.getArgRegister(0)));
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(rhtVal), regAlloc.getArgRegister(1)));
            pushCallerSaved();
            instList.add(new InstructionX86(X86Op.MOVQ, "$0", Register.RAX.getStr()));
            printCallInst = new InstructionX86(X86Op.CALL, "printf");
            printCallInst.numArgs = 2;
            instList.add(printCallInst);
            popCallerSaved();
            break;
         case "read":
            pData.insertPrintHeader(false);
            instList.add(new InstructionX86(X86Op.MOVQ, "$.LLC0", regAlloc.getArgRegister(0)));
            instList.add(new InstructionX86(X86Op.LEA, PreprocessorData.READ_LOC+"("+Register.RIP.getStr()+")", Register.RSI.getStr()));
            pushCallerSaved();
            instList.add(new InstructionX86(X86Op.MOVQ, "$0", Register.RAX.getStr()));
            InstructionX86 scanCallInst = new InstructionX86(X86Op.CALL, "scanf");
            scanCallInst.numArgs = 2;
            instList.add(scanCallInst);
            popCallerSaved();
            instList.add(new InstructionX86(X86Op.MOVQ, PreprocessorData.READ_LOC+"("+Register.RIP.getStr()+")", Register.R10.getStr()));
            if(isDot) {
               instList.add(new InstructionX86(X86Op.MOVQ, Register.R10.getStr(), "0(" + rhtVal + ")"));
            } else {
               instList.add(new InstructionX86(X86Op.MOVQ, Register.R10.getStr(), rhtVal));
            }
            break;

/******Alloc/Dealloc******/
         case "del":
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(rhtVal), regAlloc.getArgRegister(0)));
            InstructionX86 dallocCallInst = new InstructionX86(X86Op.CALL, "free");
            dallocCallInst.numArgs = 1;
            pushCallerSaved();
            instList.add(dallocCallInst);
            popCallerSaved();
            break;
         case "new":
            StructType sType = (StructType) Mini.globalStructTable.get(lftVal);
            instList.add(new InstructionX86(X86Op.MOVQ, "$"+(sType.fields.size() * MiniUtils.DSIZE), regAlloc.getArgRegister(0)));
            InstructionX86 allocCallInst = new InstructionX86(X86Op.CALL, "malloc");
            allocCallInst.numArgs = 1;
            pushCallerSaved();
            instList.add(allocCallInst);
            instList.add(new InstructionX86(X86Op.MOVQ, Register.RAX.getStr(), Register.R10.getStr()));
            popCallerSaved();
            instList.add(new InstructionX86(X86Op.MOVQ, Register.R10.getStr(), rhtVal));
            break;

/******Return********/
         case "storeret":
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(rhtVal), Register.RAX.getStr()));
            break;
         case "loadret":
            instList.add(new InstructionX86(X86Op.MOVQ, Register.RAX.getStr(), Register.R10.getStr()));
            if(midVal == null) {
               popCallerSaved();
            }
            instList.add(new InstructionX86(X86Op.MOVQ, Register.R10.getStr(), rhtVal));
            break;
         case "ret":
            break;

/******Function Call******/
         case "call":
            pushCallerSaved();
            InstructionX86 callInst = new InstructionX86(X86Op.CALL, lftVal);
            callInst.numArgs = Integer.parseInt(rhtVal);
            instList.add(callInst);
            break;

/*****Boolean ops******/
         case "or":
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(midVal), regAlloc.assign(rhtVal)));
            instList.add(new InstructionX86(X86Op.OR, regAlloc.assign(lftVal), regAlloc.assign(rhtVal)));
            break;
         case "and":
            instList.add(new InstructionX86(X86Op.MOVQ, midVal, rhtVal));
            instList.add(new InstructionX86(X86Op.AND, lftVal, rhtVal));
            break;
         case "not":
            instList.add(new InstructionX86(X86Op.NOT, regAlloc.assign(rhtVal)));
            break;

/*****Conditional*******/
         case "cmove":
            instList.add(new InstructionX86(X86Op.MOVQ, "$0", rhtVal));
            instList.add(new InstructionX86(X86Op.MOVQ, "$1", lftVal));
            instList.add(new InstructionX86(X86Op.CMOVE, lftVal, rhtVal));
            break;
         case "cmovle":
            instList.add(new InstructionX86(X86Op.MOVQ, "$0", rhtVal));
            instList.add(new InstructionX86(X86Op.MOVQ, "$1", lftVal));
            instList.add(new InstructionX86(X86Op.CMOVLE, lftVal, rhtVal));
            break;
         case "cmovge":
            instList.add(new InstructionX86(X86Op.MOVQ, "$0", rhtVal));
            instList.add(new InstructionX86(X86Op.MOVQ, "$1", lftVal));
            instList.add(new InstructionX86(X86Op.CMOVGE, lftVal, rhtVal));
            break;
         case "cmovl":
            instList.add(new InstructionX86(X86Op.MOVQ, "$0", rhtVal));
            instList.add(new InstructionX86(X86Op.MOVQ, "$1", lftVal));
            instList.add(new InstructionX86(X86Op.CMOVL, lftVal, rhtVal));
            break;
         case "cmovg":
            instList.add(new InstructionX86(X86Op.MOVQ, "$0", rhtVal));
            instList.add(new InstructionX86(X86Op.MOVQ, "$1", lftVal));
            instList.add(new InstructionX86(X86Op.CMOVG, lftVal, rhtVal));
            break;
         case "cmovne":
            instList.add(new InstructionX86(X86Op.MOVQ, "$0", rhtVal));
            instList.add(new InstructionX86(X86Op.MOVQ, "$1", lftVal));
            instList.add(new InstructionX86(X86Op.CMOVNE, lftVal, rhtVal));
            break;
         case "comp":
            instList.add(new InstructionX86(X86Op.CMP, midVal, lftVal));
            break;
         case "compi":
            instList.add(new InstructionX86(X86Op.CMP, midVal, lftVal));
            break;
         case "cbreq":
            instList.add(new InstructionX86(X86Op.JE, lftVal));
            instList.add(new InstructionX86(X86Op.JMP, rhtVal));
            break;
         case "cbrge":
            instList.add(new InstructionX86(X86Op.JGE, lftVal));
            instList.add(new InstructionX86(X86Op.JMP, rhtVal));
            break;
         case "cbrgt":
            instList.add(new InstructionX86(X86Op.JG, lftVal));
            instList.add(new InstructionX86(X86Op.JMP, rhtVal));
            break;
         case "cbrle":
            instList.add(new InstructionX86(X86Op.JLE, lftVal));
            instList.add(new InstructionX86(X86Op.JMP, rhtVal));
            break;
         case "cbrlt":
            instList.add(new InstructionX86(X86Op.JL, lftVal));
            instList.add(new InstructionX86(X86Op.JMP, rhtVal));
            break;
         case "cbrne":
            instList.add(new InstructionX86(X86Op.JNE, lftVal));
            instList.add(new InstructionX86(X86Op.JMP, rhtVal));
            break;
         case "jumpi":
            instList.add(new InstructionX86(X86Op.JMP, rhtVal));
            break;

/*****Arithmetic*******/
         case "add":
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(midVal), regAlloc.assign(rhtVal)));
            instList.add(new InstructionX86(X86Op.ADDQ, regAlloc.assign(lftVal), regAlloc.assign(rhtVal)));
            break;
         case "sub":
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(lftVal), regAlloc.assign(rhtVal)));
            instList.add(new InstructionX86(X86Op.SUBQ, regAlloc.assign(midVal), regAlloc.assign(rhtVal)));
            break;
         case "mult":
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(midVal), regAlloc.assign(rhtVal)));
            instList.add(new InstructionX86(X86Op.IMULQ, regAlloc.assign(lftVal), regAlloc.assign(rhtVal)));
            break;
         case "div":
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(lftVal), Register.RAX.getStr()));
            instList.add(new InstructionX86(X86Op.MOVQ, regAlloc.assign(lftVal), Register.RDX.getStr()));
            instList.add(new InstructionX86(X86Op.SARQ, "$63", Register.RDX.getStr()));
            instList.add(new InstructionX86(X86Op.IDIVQ, regAlloc.assign(midVal)));
            instList.add(new InstructionX86(X86Op.MOVQ, Register.RAX.getStr(), regAlloc.assign(rhtVal)));
            break;
      }
   }

   private void pushCallerSaved() {
      instList.add(new InstructionX86(X86Op.PUSHQ, Register.RAX.getStr()));
      instList.add(new InstructionX86(X86Op.PUSHQ, Register.RCX.getStr()));
      instList.add(new InstructionX86(X86Op.PUSHQ, Register.RDX.getStr()));
      instList.add(new InstructionX86(X86Op.PUSHQ, Register.RSI.getStr()));
      instList.add(new InstructionX86(X86Op.PUSHQ, Register.RDI.getStr()));
      instList.add(new InstructionX86(X86Op.PUSHQ, Register.R8.getStr()));
      instList.add(new InstructionX86(X86Op.PUSHQ, Register.R9.getStr()));
      instList.add(new InstructionX86(X86Op.PUSHQ, Register.R11.getStr()));
   }

   private void popCallerSaved() {
      instList.add(new InstructionX86(X86Op.POPQ, Register.R11.getStr()));
      instList.add(new InstructionX86(X86Op.POPQ, Register.R9.getStr()));
      instList.add(new InstructionX86(X86Op.POPQ, Register.R8.getStr()));
      instList.add(new InstructionX86(X86Op.POPQ, Register.RDI.getStr()));
      instList.add(new InstructionX86(X86Op.POPQ, Register.RSI.getStr()));
      instList.add(new InstructionX86(X86Op.POPQ, Register.RDX.getStr()));
      instList.add(new InstructionX86(X86Op.POPQ, Register.RCX.getStr()));
      instList.add(new InstructionX86(X86Op.POPQ, Register.RAX.getStr()));
   }

   public void rewriteRegisters(ArrayList<CopyEntry> copyIn) {
      if(lftVal != null) {
         Instruction inst = findCopyEntry(lftVal, copyIn);
         if(inst != null) {
            lftVal = inst.lftVal;
         }
      }

      if(midVal != null) {
         Instruction inst = findCopyEntry(midVal, copyIn);
         if(inst != null) {
            midVal = inst.lftVal;
         }
      }

      if(rhtVal != null) {
         Instruction inst = findCopyEntry(rhtVal, copyIn);
         if(inst != null) {
            rhtVal = inst.lftVal;
         }
      }
   }

   private Instruction findCopyEntry(String itemChecked, ArrayList<CopyEntry> copyIn) {
      for(CopyEntry entry : copyIn) {
         if(entry.ref != this && (entry.dest).equals(itemChecked)) {
            return entry.ref;
         }
      }
      return null;
   }

   public Instruction duplicate() {
      Instruction newInst = new Instruction(op, lftVal, midVal, rhtVal);

      newInst.parentType = parentType;
      newInst.isDot = isDot;

      return newInst;
   }

   public String toString() {
      StringBuilder builder = new StringBuilder();

      builder.append("\t" + op + " ");

      if(lftVal != null) {
         builder.append(lftVal + ", ");
      }

      if(midVal != null) {
         builder.append(midVal + ", ");
      }

      if(rhtVal != null) {
         builder.append(rhtVal + "\n");
      }

      return builder.toString();
   }

}
