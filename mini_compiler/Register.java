public enum Register {
   RAX  ("%rax"), // return, caller saved
   RBX  ("%rbx"), // callee saved

   RSP  ("%rsp"), // stack pointer
   RBP  ("%rbp"), // base pointer
   RDI  ("%rdi"), // arg1, caller saved if used
   RSI  ("%rsi"), // arg2, caller saved if used
   RDX  ("%rdx"), // arg3, caller saved
   RCX  ("%rcx"), // arg4, caller saved
   R8   ("%r8"),  // arg5, caller saved if used
   R9   ("%r9"),  // arg6, caller saved if used

   R10  ("%r10"), // caller saved if used
   R11  ("%r11"), // caller saved if used
   R12  ("%r12"), // callee saved
   R13  ("%r13"), // callee saved
   R14  ("%r14"), // callee saved
   R15  ("%r15"), // callee saved

   RIP  ("%rip")  //global var pointer, DO NOT USE
   ;

   public final String regStr;

   Register(String str) {
      this.regStr = str;
   }

   public String getStr() {
      return regStr;
   }

   public static boolean isRealReg(String str) {
      for(Register reg : Register.values()) {
         if((reg.regStr).equals(str)) {
            return true;
         }
      }
      return false;
   }
}
