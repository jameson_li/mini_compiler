tree grammar CFG_ILOCGenerator;

options
{
   tokenVocab=Mini;
   ASTLabelType=CommonTree;
}

/*
   Tree Parser -- generate CFG & ILOC of AST
*/
@header
{
   import javax.json.*;
   import java.util.LinkedHashMap;
   import java.util.LinkedList;
   import java.lang.StringBuilder;
   import java.util.Map.Entry;
}

@members
{
  private LinkedHashMap<String,Type> globalSymbolTable = null;
  private LinkedHashMap<String,Type> globalStructTable = null;
  private LinkedHashMap<String,Type> currentSymbolTable = null;
  private FunctionType curFunc = null;

  private static final String FUNC_RET_OP = "FUNCTION_";
  private static final String FUNC_RET_OP_L = "FUNCTION_L";

  private int labelCount = 1;

  private int registerCount = 0;

  private boolean isDefined(String type) {
    boolean ret = false;

    if(currentSymbolTable != null) {
       ret = currentSymbolTable.containsKey(type);
    }

    return ret ? ret : globalSymbolTable.containsKey(type);
  }

  private Type getSymbolEntry(String type) {
    if(isDefined(type)) {
      Type ret = currentSymbolTable.get(type);

      return ret == null ? globalSymbolTable.get(type) : ret;
    }

    return null;
  }

  private boolean isGlobalSymbol(String id) {
    return globalSymbolTable.containsKey(id);
  }

  private boolean isLocalSymbol(String id) {
   return currentSymbolTable.containsKey(id);
  }
}

translate[LinkedHashMap<String,Type> gst, LinkedHashMap<String,Type> gStructTable]
   returns [int numErrors = 0]
   @init { globalSymbolTable = gst; globalStructTable = gStructTable;}
   :  ^(PROGRAM t=types d=declarations[globalSymbolTable] f=functions)
      {
      }
   ;

types
   returns [int numErrors = 0]
   :  ^(TYPES (t=type_decl)*)
       { }
   |   { }
   ;

type_decl
   returns [int numErrors = 0]
   @init {}
   :  ^(ast=STRUCT
       (id=ID) { } n=nested_decl)
      {
      }
   ;

nested_decl
   :  (f=field_decl)+
      { }
   ;

field_decl
   :  ^(DECL ^(TYPE t=type) id=ID)
      {
      }
   ;

type
   :  INT {}
   |  BOOL {}
   |  ^(STRUCT id=ID)
       {
       }
   ;

declarations[LinkedHashMap<String,Type> table]
   :  ^(DECLS (d=decl_list[table])*) {}
   ;

decl_list[LinkedHashMap<String,Type> table]
   :  ^(DECLLIST ^(TYPE t=type)
         (id=ID
            {
               table.get($id.text).regNum = registerCount;
               registerCount++;
            }
         )+
      )
   ;

parameters[LinkedHashMap<String,Type> table]
   returns[Expression ex = null]
   @init
   {
      int paramNum = 0;
      ex = new Expression();
   }
   :  ^(PARAMS (p=param_decl[table, paramNum++]
                  { ex.instruct.add($p.instruct); })*)
      {
      }
   ;

param_decl[LinkedHashMap<String,Type> table, int paramNum]
   returns[Instruction instruct = null]
   :  ^(DECL ^(TYPE t=type) id=ID)
      {
         instruct = new Instruction("loadinargument", $id.text,
            Integer.toString(paramNum), "r" + registerCount);

         table.get($id.text).regNum = registerCount;
         registerCount++;
      }
   ;

functions
   :  ^(FUNCS (f=function {  })*)
       { }
   |   { }
   ;

function
   @init
   {
      LinkedList<BasicElement> statementList = new LinkedList<BasicElement>();
      registerCount = 0;
   }
   :  ^(ast=FUN (id=ID
         {
            curFunc = (FunctionType)globalSymbolTable.get($id.text);
            curFunc.returnLabel = "L" + labelCount++;
            currentSymbolTable = curFunc.localSymbolTable;
         })
         p=parameters[currentSymbolTable] r=return_type
         d=declarations[currentSymbolTable] s=statement_list[$id.text])
      {
         statementList.add($p.ex);
         statementList.addAll($s.blk.statementList);
         curFunc.basicBlock.statementList = statementList;
         curFunc.basicBlock.label = "L" + labelCount++;

         // add return block
         BasicBlock returnBlock = new BasicBlock(curFunc.returnLabel);
         Statement returnStatement = new Statement();
         returnBlock.statementList.add(returnStatement);
         returnStatement.instruct.add(new Instruction("ret", "true"));
         returnStatement.isReturnStatement = true;
         curFunc.basicBlock.statementList.add(returnBlock);

         //clear symbol table for current function
         currentSymbolTable = null;
         curFunc = null;
      }
   ;

return_type
   :  ^(RETTYPE rtype)
      {
      }
   ;

rtype
   :  t=type { }
   |  VOID { }
   ;

statement
   returns [BasicElement elem = null]
   :  (a=return_stmt | a=assignment | a=invocation_stmt | a=print | a=read | a=delete)
      {
         elem = $a.elem;
      }
   |  b=block
      {
         elem = $b.elem;
      }
   |  c=conditional
      {
         elem = $c.elem;
      }
   |  d=loop
      {
         elem = $d.elem;
      }
   ;

block
   returns [BasicBlock elem = null]
   :  ^(BLOCK s=statement_list["L" + labelCount++])
      {
         elem = $s.blk;
         elem.afterLabel = "L" + labelCount++;
      }
   ;

statement_list[String label]
   returns [BasicBlock blk = null]
   @init { blk = new BasicBlock(label); }
   :  ^(STMTS (s=statement { blk.addStatement($s.elem); })*)
      {

      }
   ;

assignment
   returns [Statement elem = null]
   :  ^(ast=ASSIGN e=expression l=lvalue)
      {
         elem = new Statement();
         elem.instruct.addAll($e.exp.instruct);
         elem.instruct.addAll($l.exp.instruct);

         if($l.exp.isDot) {
            Instruction parentInstruct = elem.instruct.getLast();

            Instruction getFieldInst = elem.instruct.getLast();
            String lValDestReg = getFieldInst.rhtVal;
            String expResultReg = $e.exp.instruct.getLast().rhtVal;

            getFieldInst.op = "loadaia";

            Instruction nI = new Instruction("storeai", expResultReg,
               lValDestReg, "@"+$l.exp.fieldId);
            elem.instruct.add(nI);

            nI.parentType = parentInstruct.parentType;
         } else if($l.exp.globalId != null) {
            String expResultReg = $e.exp.instruct.getLast().rhtVal;
            elem.instruct.add(new Instruction("storeglobal", expResultReg,
               $l.exp.globalId));
         } else {
            String lValDestReg = $l.exp.instruct.getLast().rhtVal;
            String expResultReg = $e.exp.instruct.getLast().rhtVal;

            elem.instruct.add(
               new Instruction("mov", expResultReg, lValDestReg));
         }
      }
   ;

print
   returns[Statement elem = new Statement()]
   @init
   {
      boolean endl = false;
   }
   :  ^(ast=PRINT e=expression (ENDL {endl = true;})?)
      {
         elem.instruct.addAll($e.exp.instruct);

         String integerRegister = $e.exp.instruct.getLast().rhtVal;
         elem.instruct.add(new Instruction(endl ? "println" : "print", integerRegister));
      }
   ;

read
   returns [Statement elem = new Statement()]
   :  ^(ast=READ l=lvalue)
      {
         String lValueResult;

         elem.instruct.addAll($l.exp.instruct);
         if($l.exp.isDot) {
            Instruction parentInstruct = elem.instruct.getLast();

            Instruction getFieldInst = elem.instruct.getLast();
            String lValDestReg = getFieldInst.rhtVal;

            getFieldInst.op = "loadaia";

            lValueResult = lValDestReg;

            getFieldInst.parentType = parentInstruct.parentType;

            Instruction readInst = new Instruction("read", lValueResult);
            readInst.isDot = true;
            elem.instruct.add(readInst);
         } else if($l.exp.globalId != null) {
            lValueResult = $l.exp.globalId;
            elem.instruct.add(new Instruction("read", lValueResult));
         } else {
            lValueResult = $l.exp.instruct.getLast().rhtVal;
            elem.instruct.add(new Instruction("read", lValueResult));
         }

      }
   ;

conditional
   returns [ConditionalBlock elem = new ConditionalBlock()]
   :  ^(ast=IF
         g=expression
         t=block
         {
            elem.thenBlock = $t.elem;
         }
         (e=block
         {
            elem.elseBlock = $e.elem;
         })?)
      {
         elem.guard.instruct = $g.exp.instruct;

         // allow for lone function as conditional
         if($g.exp.tempOpCode != null && $g.exp.tempOpCode.equals(FUNC_RET_OP)) {
            int lreg = registerCount;

            elem.guard.instruct.addAll($g.exp.instruct);
            elem.guard.instruct.add(new Instruction("loadi", "1", "r"+lreg));
            elem.guard.instruct.add(new Instruction("comp", "r"+(registerCount - 1), "r"+lreg, "r"+registerCount++));

            registerCount++;

            $g.exp.tempOpCode = "cbreq";
         } else if($g.exp.tempOpCode != null && $g.exp.tempOpCode.equals(FUNC_RET_OP_L)) {
            Instruction inst = $g.exp.instruct.getLast();
            int lreg = registerCount++;

            elem.guard.instruct.addAll($g.exp.instruct);
            elem.guard.instruct.add(new Instruction("loadi", "1", "r"+lreg));
            elem.guard.instruct.add(new Instruction("comp", inst.rhtVal, "r"+lreg, "r"+registerCount++));
            $g.exp.tempOpCode = "cbreq";
         }

         if(elem.elseBlock != null) {
            elem.guard.instruct.add(
               new Instruction($g.exp.tempOpCode, elem.thenBlock.label, elem.elseBlock.label));

            Statement jStatement = new Statement();
            jStatement.instruct.add(new Instruction("jumpi", "L" + labelCount));
            elem.thenBlock.statementList.add(jStatement);
            elem.afterLabel = "L" + labelCount++;

         } else {
            elem.guard.instruct.add(
               new Instruction($g.exp.tempOpCode, elem.thenBlock.label, elem.thenBlock.afterLabel));
         }
      }
   ;

loop
   returns [LoopBlock elem = new LoopBlock()]
   :  ^(ast=WHILE e=expression b=block expression)
      {
         elem.guard = $e.exp;

         elem.guardLabel = "L" + labelCount++;
         elem.afterLabel = "L" + labelCount++;

         elem.skipJumpStatements.add(
            new Instruction($e.exp.tempOpCode, $b.elem.label, elem.afterLabel));
         elem.block = $b.elem;

         elem.returnJumpStatements.add(new Instruction("jumpi", elem.guardLabel));
      }
   ;

delete
   returns [Statement elem = new Statement()]
   :  ^(ast=DELETE e=expression)
      {
         String delReg = $e.exp.instruct.getLast().rhtVal;
         elem.instruct.add(new Instruction("del", delReg));
      }
   ;

return_stmt
   returns [Statement elem = null]
   :  ^(ast=RETURN (e=expression)?)
      {
         Instruction loadInstruct = null;

         elem = new Statement();

         // instruct that loads return value into register
         if($e.exp != null) {
            loadInstruct = $e.exp.instruct.getLast();

            elem.instruct = $e.exp.instruct;
            elem.instruct.add(new Instruction("storeret", loadInstruct.rhtVal));
         }

         elem.instruct.add(new Instruction("jumpi", curFunc.returnLabel));
      }
   ;

invocation_stmt
   returns [Statement elem = new Statement()]
   @init
   {
      int argCounter = 0;
      ArrayList<String> storeOutArgs = new ArrayList<String>();
   }
   :  ^(INVOKE id=ID
          ^(ARGS (e=expression
                  {
                     Instruction argStore = $e.exp.instruct.getLast();
                     elem.instruct.addAll($e.exp.instruct);
                     storeOutArgs.add(argStore.rhtVal);
                  })*))
      {
         for(int i=0; i<storeOutArgs.size(); i++) {
            elem.instruct.add(new Instruction("storeoutargument", storeOutArgs.get(i), Integer.toString(i)));
            argCounter++;
         }
         elem.instruct.add(new Instruction("call", $id.text, Integer.toString(argCounter)));
         elem.instruct.add(new Instruction("loadret", "r" + registerCount++));

         elem.type = ((FunctionType)getSymbolEntry($id.text)).ret_type;
      }
   ;

invocation_exp
   returns [Statement elem = new Statement()]
   @init
   {
      int argCounter = 0;
      ArrayList<String> storeOutArgs = new ArrayList<String>();
   }
   :  ^(INVOKE id=ID
          ^(ARGS (e=expression
                  {
                     Instruction argStore = $e.exp.instruct.getLast();
                     elem.instruct.addAll($e.exp.instruct);
                     storeOutArgs.add(argStore.rhtVal);
                  })*))
      {
         for(int i=0; i<storeOutArgs.size(); i++) {
            elem.instruct.add(new Instruction("storeoutargument", storeOutArgs.get(i), Integer.toString(i)));
            argCounter++;
         }

         elem.instruct.add(new Instruction("call", $id.text, Integer.toString(argCounter)));
         elem.instruct.add(new Instruction("loadret", "r" + registerCount++));

         elem.type = ((FunctionType)getSymbolEntry($id.text)).ret_type;
         elem.tempOpCode = FUNC_RET_OP;
      }
   ;

lvalue
   returns [Expression exp = new Expression()]
   @init
   {
      LinkedList<Instruction> instruct = new LinkedList<Instruction>();
   }
   :  id=ID
      {
         if(isLocalSymbol($id.text)) {
            int idRegNum = getSymbolEntry($id.text).regNum;
            instruct.add(new Instruction("mov", "r" + idRegNum,
               "r" + idRegNum));
            exp.tempOpCode = "mov";
         } else {
            exp.globalId = $id.text;
            exp.tempOpCode = "storeglobal";
         }

         exp.instruct = instruct;
         exp.type = getSymbolEntry($id.text).type;
      }
   |  ^(ast=DOT l=lvalue id=ID)
      {
         instruct.addAll($l.exp.instruct);

         if($l.exp.globalId != null) {
            String parentReg = "r"+registerCount++;

            instruct.add(new Instruction("loadglobal", $l.exp.globalId,
               parentReg));

            Instruction nI = new Instruction("loadai", parentReg, "@"+$id.text,
               "r"+registerCount++);
            instruct.add(nI);

            nI.parentType = getSymbolEntry($l.exp.globalId).type;
            exp.type = ((StructType)Mini.globalStructTable.get(nI.parentType)).getField($id.text);
         } else {
            String parentReg = $l.exp.instruct.getLast().rhtVal;

            Instruction nI = new Instruction("loadai", parentReg,
               "@"+$id.text, "r"+registerCount++);
            instruct.add(nI);

            nI.parentType = $l.exp.type;
            exp.type = ((StructType)Mini.globalStructTable.get(nI.parentType)).getField($id.text);
         }

         exp.isDot = true;
         exp.fieldId = $id.text;
         exp.instruct = instruct;
      }
   ;

expression
   returns [Expression exp = new Expression()]
   @init
   {
      String instructStr = null;
      String instructStrH = null;
      LinkedList<Instruction> instruct = new LinkedList<Instruction>();
      exp.instruct = instruct;
   }
   :  ^(((ast=AND {instructStr = "and";}) | (ast=OR {instructStr = "or";})) lft=expression rht=expression)
      {
         String opResultReg = "r" + registerCount++;
         Instruction lftReg = $lft.exp.instruct.getLast();
         instruct.addAll($lft.exp.instruct);
         Instruction rhtReg = $rht.exp.instruct.getLast();
         instruct.addAll($rht.exp.instruct);
         instruct.add(new Instruction(instructStr, lftReg.rhtVal, rhtReg.rhtVal, opResultReg));

         // if and or or op resulted in true
         instruct.add(new Instruction("loadi", "1", "r"+registerCount));
         instruct.add(new Instruction("compi", opResultReg, "r"+registerCount++, "ccr"));
         exp.tempOpCode = "cbreq";
      }
   |  ^(((ast=EQ {instructStr = "cbreq"; instructStrH = "cmove";}) | (ast=GE {instructStr = "cbrge"; instructStrH = "cmovge";}) | (ast=GT {instructStr = "cbrgt"; instructStrH = "cmovg";}) |
         (ast=LE {instructStr = "cbrle"; instructStrH = "cmovle";}) | (ast=LT {instructStr = "cbrlt"; instructStrH = "cmovl";}) | (ast=NE {instructStr = "cbrne"; instructStrH = "cmovne";})
        ) lft=expression rht=expression)
      {
         Instruction lftInst = $lft.exp.instruct.getLast();
         instruct.addAll($lft.exp.instruct);
         Instruction rhtInst = $rht.exp.instruct.getLast();
         instruct.addAll($rht.exp.instruct);
         int cReg = registerCount++;
         instruct.add(new Instruction("comp", lftInst.rhtVal, rhtInst.rhtVal, "r" + cReg));
         instruct.add(new Instruction(instructStrH, "r"+registerCount++, "r" + cReg));
         exp.tempOpCode = instructStr;
      }
   |  ^(((ast=PLUS {instructStr = "add";}) | (ast=MINUS {instructStr = "sub";}) |
         (ast=TIMES {instructStr = "mult";}) | (ast=DIVIDE {instructStr = "div";})
        ) lft=expression rht=expression)
      {
         Instruction lftInst = $lft.exp.instruct.getLast();
         instruct.addAll($lft.exp.instruct);
         Instruction rhtInst = $rht.exp.instruct.getLast();
         instruct.addAll($rht.exp.instruct);
         instruct.add(new Instruction(instructStr, lftInst.rhtVal, rhtInst.rhtVal, "r" + registerCount++));
      }
   |  ^(ast=NOT p=expression)
      {
         Instruction reg = $p.exp.instruct.getLast();
         instruct.addAll($p.exp.instruct);

         instruct.add(new Instruction("not", reg.rhtVal));
         exp.tempOpCode = $p.exp.tempOpCode;
      }
   |  ^(ast=NEG p=expression)
      {
         Instruction num = $p.exp.instruct.getLast();
         instruct.addAll($p.exp.instruct);

         instruct.add(new Instruction("loadi", "0", "r" + registerCount));
         instruct.add(new Instruction("sub", "r" + registerCount, num.rhtVal, "r" + (registerCount + 1)));

         registerCount += 2;
      }
   |  ^(ast=DOT ex=expression id=ID)
      {
         String parentReg = $ex.exp.instruct.getLast().rhtVal;
         instruct.addAll($ex.exp.instruct);

         Instruction nI = new Instruction("loadai", parentReg,
            "@"+$id.text, "r"+registerCount++);
         instruct.add(nI);

         nI.parentType = $ex.exp.type;
         exp.type = ((StructType)Mini.globalStructTable.get(nI.parentType)).getField($id.text);
      }
   |  e=invocation_exp
      {
         instruct.addAll($e.elem.instruct);
         exp.type = $e.elem.type;
         exp.tempOpCode = $e.elem.tempOpCode;
      }
   |  id=ID
      {
         if(isLocalSymbol($id.text)) {
            int idRegNum = getSymbolEntry($id.text).regNum;
            instruct.add(new Instruction("mov", "r" + idRegNum,
               "r" + idRegNum));
         } else {
            instruct.add(new Instruction("loadglobal", $id.text,
               "r"+registerCount++));
         }

         exp.type = getSymbolEntry($id.text).type;
         exp.tempOpCode = FUNC_RET_OP_L;
      }
   |  i=INTEGER
      {
         instruct.add(new Instruction("loadi", $i.text, "r" + registerCount++));
      }
   |  ast=TRUE
      {
         instruct.add(new Instruction("loadi", "1", "r" + registerCount));
         exp.tempOpCode = FUNC_RET_OP;
         //instruct.add(new Instruction("compi", "r" + registerCount++, "1", "ccr"));
         //exp.tempOpCode = "cbreq";
      }
   |  ast=FALSE
      {
         instruct.add(new Instruction("loadi", "0", "r" + registerCount));
         exp.tempOpCode = FUNC_RET_OP;
         //instruct.add(new Instruction("compi", "r" + registerCount++, "0", "ccr"));
         //exp.tempOpCode = "cbreq";
      }
   |  ^(ast=NEW id=ID)
      {
         LinkedHashMap<String,String> structFields = ((StructType)globalStructTable.get($id.text)).fields;
         StringBuilder builder = new StringBuilder();

         builder.append("[");

         int sTableLength = structFields.size();
         for (Entry<String,String> entry : structFields.entrySet()) {
            String fieldName = entry.getKey();

            if(sTableLength-- == 1) {
               builder.append(fieldName + "]");
            } else {
               builder.append(fieldName + ", ");
            }
         }

         instruct.add(new Instruction("new", $id.text, builder.toString(), "r" + registerCount++));
      }
   |  ast=NULL
      {
         instruct.add(new Instruction("loadi", "0", "r" + registerCount++));
      }
   ;
