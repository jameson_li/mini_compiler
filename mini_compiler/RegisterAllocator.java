import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;
import java.util.LinkedList;

public class RegisterAllocator {

   public boolean replaceMode = false;
   public boolean uselessCode = false;
   public boolean copyProp = false;
   public boolean functionInline = false;

   //singleton
   private static RegisterAllocator instance = null;

   public ArrayList<String> argRegisters;
   public int spillSize = 0;

   public RegisterAllocator() {
      argRegisters = new ArrayList<String>();
      initArgRegsiters();
   }

   public static RegisterAllocator getInstance() {
      if(instance == null) {
         instance = new RegisterAllocator();
      }

      return instance;
   }

   private void initArgRegsiters() {
      // arguments 0 - 5 will be placed in this order
      String[] argArray = {Register.RDI.getStr(), Register.RSI.getStr(), Register.RDX.getStr(),
                           Register.RCX.getStr(), Register.R8.getStr(), Register.R9.getStr()};
      argRegisters.addAll(Arrays.asList(argArray));
   }

   public String getArgRegister(int argNum) {
      return argRegisters.get(argNum);
   }

   @SuppressWarnings("unchecked")
   public ArrayList<ArrayList<String>> liveAnalysis(ArrayList<InstructionX86> instList) {
      ArrayList<String> gen = new ArrayList<String>();
      ArrayList<String> kill = new ArrayList<String>();
      ArrayList<ArrayList<String>> ret = new ArrayList<ArrayList<String>>(2);

      ArrayList<InstructionX86> instListRev = (ArrayList<InstructionX86>) instList.clone();
      Collections.reverse(instListRev);

      ret.add(gen);
      ret.add(kill);

      for(InstructionX86 inst : instListRev) {
         ArrayList<ArrayList<String>> tempList = liveAnalysisEach(inst);
         gen.removeAll(tempList.get(1));
         gen.removeAll(tempList.get(0));
         gen.addAll(tempList.get(0));

         //remove then add makes sure that there is only one occurance
         kill.removeAll(tempList.get(1));
         kill.addAll(tempList.get(1));
      }

      return ret;
   }

   public ArrayList<ArrayList<CopyEntry>> copyPropagationAnalysis(LinkedList<Instruction> instList) {
      ArrayList<CopyEntry> gen = new ArrayList<CopyEntry>();
      ArrayList<CopyEntry> kill = new ArrayList<CopyEntry>();
      ArrayList<ArrayList<CopyEntry>> ret = new ArrayList<ArrayList<CopyEntry>>(2);

      ret.add(gen);
      ret.add(kill);

      for(Instruction inst : instList) {
         ArrayList<ArrayList<CopyEntry>> tempList = copyPropagationAnalysisEach(inst);
         gen.removeAll(tempList.get(0));
         gen.addAll(tempList.get(0));

         //remove then add makes sure that there is only one occurance
         kill.removeAll(tempList.get(1));
         kill.addAll(tempList.get(1));
      }

      return ret;
   }


   public ArrayList<ArrayList<CopyEntry>> copyPropagationAnalysisEach(Instruction inst) {
      ArrayList<CopyEntry> gen = new ArrayList<CopyEntry>();
      ArrayList<CopyEntry> kill = new ArrayList<CopyEntry>();
      ArrayList<ArrayList<CopyEntry>> ret = new ArrayList<ArrayList<CopyEntry>>(2);

      ret.add(gen);
      ret.add(kill);

      switch(inst.op) {
         case "mov":
            gen.add(new CopyEntry(inst.rhtVal, inst.lftVal, inst));
            break;
         case "not":
         case "compi":
         case "comp":
         case "read":
         case "loadglobal":
         case "loadi":
         case "loadai":
         case "loadret":
         case "loadinargument":
         case "sub":
         case "add":
         case "mult":
         case "div":
         case "new":
         case "and":
         case "or":
            kill.add(new CopyEntry(inst.rhtVal, null, inst));
            break;

         case "cmove":
         case "cmovle":
         case "cmovge":
         case "cmovl":
         case "cmovg":
         case "cmovne":
            kill.add(new CopyEntry(inst.lftVal, null, inst));
            kill.add(new CopyEntry(inst.rhtVal, null, inst));
            break;
      }

      return ret;
   }

   public ArrayList<ArrayList<String>> liveAnalysisEach(InstructionX86 inst) {
      ArrayList<String> gen = new ArrayList<String>();
      ArrayList<String> kill = new ArrayList<String>();
      ArrayList<ArrayList<String>> ret = new ArrayList<ArrayList<String>>(2);

      ret.add(gen);
      ret.add(kill);

      switch(inst.op) {
         case IMULQ:
         case SUBQ:
         case ADDQ:
         case AND:
         case OR:
            setDestination(inst, kill);
            setSource(inst, gen, 0);
            setSource(inst, gen ,1);
         case LEA:
         case MOVQ:
            setDestination(inst, kill);
            setSource(inst, gen, 0);
            if(inst.rhtVal != null && inst.rhtVal.matches("[0-9]+[(]r[0-9]+[)]")) {
               setSource(inst, gen, 1); // cover case when destination is an offset on stack
            }
            break;
         case NOT:
            setDestination(inst, kill);
            setSource(inst, gen, 1);
            break;
         case CMP:
            setSource(inst, gen, 0);
            setSource(inst, gen, 1);
            break;
         case IDIVQ:
         case SARQ:
            setSource(inst, gen, 1);
            break;
         case CALL:
            setCallSource(inst.numArgs, gen);
            setCallSource(inst.numArgs, kill);
            // setCallerSaved(gen); // Consider changing the name of this function
            // setCallerSaved(kill);
            break;
         case CMOVE:
         case CMOVLE:
         case CMOVGE:
         case CMOVL:
         case CMOVG:
         case CMOVNE:
            setSource(inst, gen, 0);
            setSource(inst, gen, 1);
            setDestination(inst, kill);
            break;
      }

      return ret;
   }

   public void setDestination(InstructionX86 inst, ArrayList<String> kill) {
      if(inst.getDestinationReg() != null) {
         String dest1 = inst.getDestinationReg();
         kill.removeAll(Arrays.asList(dest1));
         kill.add(dest1);
      }
   }

   // src(0) is lft reg, src(1) is rht reg
   public void setSource(InstructionX86 inst, ArrayList<String> gen, int sourceNum) {
      if(inst.getSourceReg()[sourceNum] != null) {
         String src1 = inst.getSourceReg()[sourceNum];

         gen.removeAll(Arrays.asList(src1));
         gen.add(src1);
      }
   }

   public void setCallerSaved(ArrayList<String> list) {
      List<String> targetList = Arrays.asList(Register.RAX.regStr);

      list.removeAll(targetList);
      list.addAll(targetList);
   }

   public void setCallSource(int numArgs, ArrayList<String> gen) {
      List<String> sourceList = Arrays.asList(Register.RDI.regStr, Register.RSI.regStr,
         Register.RDX.regStr, Register.RCX.regStr, Register.R8.regStr, Register.R9.regStr);

      if(numArgs > sourceList.size()) {
         MiniUtils.error("more than 6 arguments function call not supported");
      }

      for(int i=0; i<numArgs; i++) {
         gen.removeAll(Arrays.asList(sourceList.get(i)));
         gen.add(sourceList.get(i));
      }
   }

   // used for testing
   public String assign(String reg) {

      if(reg.matches("r[0-9]+") && replaceMode && false) {
         switch(reg) {
            case "r0":
               return "%r12";
            case "r1":
               return "%r13";
            case "r2":
               return "%r14";
            case "r3":
               return "%r15";
            default:
               return "INSUFFICIENT REGISTERS";
         }
      } else if(replaceMode && false) {
         System.out.println("Log: Trying to assign register to " + reg);
         return reg;
      }

      return reg;
   }
}
