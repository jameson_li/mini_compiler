import java.util.ArrayList;
import java.util.List;

import java.lang.StringBuilder;

public class MiniUtils {

   public static final int DSIZE = 8;
   public static final Register H_REG = Register.R10;
   public static final String READ_LOC = "_READ_LOC";

   public static void error(String errMsg) {
      System.err.println(errMsg);
      System.exit(1);
   }

   public static ArrayList<String> calculateLiveOut(BasicElement pred) {
      ArrayList<String> liveout = new ArrayList<String>();

      if(pred != null) {
         //lo(pred) - kill(pred)
         liveout.addAll(pred.liveout);
         liveout.removeAll(pred.kill);

         // gen(pred) U (liveout)
         liveout.removeAll(pred.gen);
         liveout.addAll(pred.gen);
      }

      return liveout;
   }

   public static ArrayList<CopyEntry> calculateCopyPropagation(BasicElement pred) {
      ArrayList<CopyEntry> copyIn = new ArrayList<CopyEntry>();

      if(pred != null) {
         copyIn.addAll(pred.copyIn);
         copyIn.removeAll(pred.copyInKill);

         // union
         copyIn.removeAll(pred.copyInGen);
         copyIn.addAll(pred.copyInGen);
      }

      return copyIn;
   }

   // intersection of two lists
   public static <T> ArrayList<T> intersection(ArrayList<T> list1, ArrayList<T> list2) {
      ArrayList<T> ret = new ArrayList<T>(list1);

      ret.retainAll(list2);

      return ret;
   }

   // union of two lists
   public static <T> ArrayList<T> union(ArrayList<T> list1, ArrayList<T> list2) {
      ArrayList<T> ret = new ArrayList<T>();

      if(list1 != null) {
         ret = new ArrayList<T>(list1);
      }

      if(list2 != null) {
         ret.removeAll(list2);
         ret.addAll(list2);
      }

      return ret;
   }

   public static String dumpArrayList(ArrayList<String> list) {
      StringBuilder builder = new StringBuilder();
      if(list != null) {
         for(String i : list) {
            builder.append(i + ", ");
         }
      }

      return builder.toString();
   }
}
