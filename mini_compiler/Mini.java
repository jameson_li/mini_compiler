import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import org.antlr.stringtemplate.*;

import java.io.*;
import java.util.Vector;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import javax.json.JsonValue;
import javax.json.Json;
import java.lang.StringBuilder;
import java.io.File;

public class Mini
{
   public static void main(String[] args)
   {
      parseParameters(args);

      CommonTokenStream tokens = new CommonTokenStream(createLexer());
      MiniParser parser = new MiniParser(tokens);
      CommonTree tree = parse(parser);


      if (_displayAST && tree != null)
      {
         DOTTreeGenerator gen = new DOTTreeGenerator();
         StringTemplate st = gen.toDOT(tree);
         System.out.println(st);
      }

      if(_dumpJSON && !parser.hasErrors()) {
         JsonValue json = translate(tree, tokens);
         System.out.println(json);
      }

      checkSemantics(tree, tokens);

      if(!containErrors) {
         generateCfgIloc(globalSymbolTable, globalStructTable, tree, tokens);
         if(_dumpX86_64) {
            outputX86_64(globalSymbolTable);
         }
      }
   }

   private static final String DISPLAYAST = "-displayAST";
   private static final String DEBUG = "-debug";
   private static final String CFGILOC = "-dumpIL";
   private static final String JSONOUT = "-dumpJSON";
   private static final String SOUT = "-s";
   private static final String REGREPLACE = "-r";
   private static final String USELESSCODE = "-ucr";
   private static final String COPYPROPAGATION = "-cp";
   private static final String FUNCTIONINLINE = "-fi";

   private static String _inputFile = null;
   private static String _outputFileIL = null;
   private static String _outputFileX86_64 = null;

   private static boolean _displayAST = false;
   private static boolean _debugMode = false;
   private static boolean _CFG_ILOC = false;
   private static boolean _dumpJSON = false;
   private static boolean _dumpX86_64 = false;
   private static boolean containErrors = false;

   private static LinkedHashMap<String,Type> globalSymbolTable = null;
   public static LinkedHashMap<String,Type> globalStructTable = null;

   private static void parseParameters(String [] args)
   {
      for (int i = 0; i < args.length; i++)
      {
         if (args[i].equals(DISPLAYAST))
         {
            _displayAST = true;
         }
         else if(args[i].equals(DEBUG))
         {
            _debugMode = true;
         }
         else if(args[i].equals(CFGILOC))
         {
            _CFG_ILOC = true;
         }
         else if(args[i].equals(JSONOUT)) {
            _dumpJSON = true;
         }
         else if(args[i].equals(SOUT)) {
            _dumpX86_64 = true;
         }
         else if(args[i].equals(REGREPLACE)) {
            RegisterAllocator.getInstance().replaceMode = true;
         }
         else if(args[i].equals(USELESSCODE)) {
            RegisterAllocator.getInstance().uselessCode = true;
         }
         else if(args[i].equals(COPYPROPAGATION)) {
            RegisterAllocator.getInstance().copyProp = true;
         }
         else if(args[i].equals(FUNCTIONINLINE)) {
            RegisterAllocator.getInstance().functionInline = true;
         }
         else if (args[i].charAt(0) == '-')
         {
            System.err.println("unexpected option: " + args[i]);
            System.exit(1);
         }
         else if (_inputFile != null)
         {
            System.err.println("too many files specified");
            System.exit(1);
         }
         else
         {
            _inputFile = args[i];

            StringBuilder builder = new StringBuilder(_inputFile);
            builder.replace(builder.length() - 4, builder.length(), "il");
            _outputFileIL = builder.toString();

            builder = new StringBuilder(_inputFile);
            builder.replace(builder.length() - 4, builder.length(), "s");
            _outputFileX86_64 = builder.toString();
         }
      }
   }

   private static CommonTree parse(MiniParser parser)
   {
      try
      {
         MiniParser.program_return ret = parser.program();

         return (CommonTree)ret.getTree();
      }
      catch (org.antlr.runtime.RecognitionException e)
      {
         MiniUtils.error(e.toString());
      }
      catch (Exception e)
      {
         System.exit(-1);
      }

      return null;
   }

   private static JsonValue translate(CommonTree tree, CommonTokenStream tokens)
   {
      try
      {
         CommonTreeNodeStream nodes = new CommonTreeNodeStream(tree);
         nodes.setTokenStream(tokens);
         ToJSON tparser = new ToJSON(nodes);

         return tparser.translate();
      }
      catch (org.antlr.runtime.RecognitionException e)
      {
         MiniUtils.error(e.toString());
      }
      return Json.createObjectBuilder().build();
   }

   private static void checkSemantics(CommonTree tree, CommonTokenStream tokens)
   {
      try
      {
         CommonTreeNodeStream nodes = new CommonTreeNodeStream(tree);
         nodes.setTokenStream(tokens);
         CheckSemantics tparser = new CheckSemantics(nodes);

         tparser.translate();
         globalSymbolTable = tparser.globalSymbolTable;
         globalStructTable = tparser.globalStructTable;

         if(tparser.hasErrors()) {
            containErrors = true;
            System.out.println(tparser.getErrorMessage());
         }
      }
      catch (org.antlr.runtime.RecognitionException e)
      {
         MiniUtils.error(e.toString());
      }
   }

   private static void generateCfgIloc(LinkedHashMap<String,Type> gst, LinkedHashMap<String,Type> gStructTable,
                                       CommonTree tree, CommonTokenStream tokens) {
      try
      {
         CommonTreeNodeStream nodes = new CommonTreeNodeStream(tree);
         nodes.setTokenStream(tokens);
         CFG_ILOCGenerator gen = new CFG_ILOCGenerator(nodes);

         gen.translate(gst, gStructTable);

         if(_CFG_ILOC) {
            outputILOC();
         }
      }
      catch (org.antlr.runtime.RecognitionException e)
      {
         MiniUtils.error(e.toString());
      }
   }

   //X86_64
   private static void outputX86_64(LinkedHashMap<String,Type> gst) {
      File file = new File(_outputFileX86_64);

      try {
         if(!file.exists()) {
            file.createNewFile();
         }
         PrintWriter writer = new PrintWriter(file);
         PreprocessorData pData = PreprocessorData.getInstance();
         String[] fileName = _inputFile.split("/");

         pData.addFileName(fileName[fileName.length - 1]);

         for (Entry<String, Type> entry : globalSymbolTable.entrySet()) {
            Type type = entry.getValue();

            if(!(type instanceof FunctionType)) {
               pData.addComm(type.id);
            }
         }

         pData.addComm(PreprocessorData.READ_LOC);

         writer.println(pData.getString());
         pData.clearString();

         for(Entry<String, Type> entry : globalSymbolTable.entrySet()) {
            Type type = entry.getValue();

            if(type instanceof FunctionType) {
               FunctionType funct = (FunctionType)type;
               funct.genX86_64Analysis();
               writer.println(funct.getX86_64());
            }
         }
         writer.close();
      } catch(Exception e) {
         e.printStackTrace();
         MiniUtils.error(e.toString());
      }
   }

   //ILOC
   private static void outputILOC() {
      File file = new File(_outputFileIL);

      try {
         if(!file.exists()) {
            file.createNewFile();
         }
         PrintWriter writer = new PrintWriter(file);

         for (Entry<String, Type> entry : globalSymbolTable.entrySet()) {
            Type type = entry.getValue();

            if (type instanceof FunctionType) {
               FunctionType funct = (FunctionType)type;
               funct.genAnalysis(globalSymbolTable);
               writer.println(funct.toString());
            }
         }
         writer.close();
      }
      catch (Exception e) {
         MiniUtils.error(e.toString());
      }
   }

   private static MiniLexer createLexer()
   {
      try
      {
         ANTLRInputStream input;
         if (_inputFile == null)
         {
            input = new ANTLRInputStream(System.in);
         }
         else
         {
            input = new ANTLRInputStream(
               new BufferedInputStream(new FileInputStream(_inputFile)));
         }
         return new MiniLexer(input);
      }
      catch (java.io.IOException e)
      {
         System.err.println("file not found: " + _inputFile);
         System.exit(1);
         return null;
      }
   }
}
