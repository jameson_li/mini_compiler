import java.util.ArrayList;
import java.util.Arrays;

public class PreprocessorData {

   public static final String READ_LOC = MiniUtils.READ_LOC;

   //singleton
   private static PreprocessorData instance = null;

   private StringBuilder builder;

   private boolean lc0 = false;
   private boolean lc1 = false;

   public PreprocessorData() {
      builder = new StringBuilder();
      lc0 = false;
      lc1= false;
   }

   public static PreprocessorData getInstance() {
      if(instance == null) {
         instance = new PreprocessorData();
      }

      return instance;
   }

   public void addToData(String str) {
      builder.append(str);
   }

   public String getString() {
      return builder.toString();
   }

   public void clearString() {
      builder = new StringBuilder();
   }

   public void insertPrintHeader(boolean newLine) {
      // only allow directive to print once
      if((newLine && !lc1) || (!newLine && !lc0)) {
         builder.append("\t.section\t.rodata\n");

         if(newLine) {
            lc1 = true;
            builder.append(".LLC1:\n\t.string \"%ld\\n\"\n");
         } else {
            lc0 = true;
            builder.append(".LLC0:\n\t.string \"%ld\"\n");
         }
      }
   }

   public void addFileName(String name) {
      builder.append("\t.file   \"" + name + "\"\n");
   }

   public void addFunctionHeader(String id) {
      builder.append("\t.text\n");
      builder.append("\t.globl   " + id + "\n");
      builder.append("\t.type   " + id + ", @function\n");
   }

   public void addComm(String id) {
      builder.append("\t.comm   " + id + ",8,8\n");
   }
}
