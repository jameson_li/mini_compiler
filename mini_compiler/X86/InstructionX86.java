import java.lang.StringBuilder;

import java.util.ArrayList;
import java.util.HashMap;

public class InstructionX86 extends BasicElement {

   public X86Op op;

   public String lftVal; //left register or immediate value
   public String midVal; //right register
   public String rhtVal; //destination register or source register

   private RegisterAllocator regAlloc = RegisterAllocator.getInstance();

   public int numArgs = 0;

   public InstructionX86(X86Op op, String rhtVal) {
      this.op = op;
      this.rhtVal = rhtVal;

      setReg();
   }

   public InstructionX86(X86Op op, String lftVal, String rhtVal) {
      this.op = op;
      this.lftVal = lftVal;
      this.rhtVal = rhtVal;

      setReg();
   }

   public InstructionX86(X86Op op, String lftVal, String midVal, String rhtVal) {
      this.op = op;
      this.lftVal = lftVal;
      this.midVal = midVal;
      this.rhtVal = rhtVal;

      setReg();
   }

   public BasicElement calculateLiveNow(BasicElement pred) {
      ArrayList<ArrayList<String>> analysisResult = regAlloc.liveAnalysisEach(this);
      gen = analysisResult.get(0);
      kill = analysisResult.get(1);

      oldLiveout = liveout;
      liveout = MiniUtils.calculateLiveOut(pred);

      return this;
   }

   public void removeUselessCodeX86(ArrayList<InstructionX86> instList) {
      if(op == X86Op.MOVQ) {
         String dest = getDestinationReg();
         if (dest != null && !liveout.contains(dest) &&
               !dest.equals(Register.RAX.getStr()) && !dest.contains("%")) {
            instList.remove(this);
         }
      }
   }

   public String toString() {
      StringBuilder builder = new StringBuilder();

      builder.append("\t" + op.getStr() + " ");

      if(lftVal != null) {
         builder.append(lftVal + ", ");
      }

      if(midVal != null) {
         builder.append(midVal + ", ");
      }

      if(rhtVal != null) {
         builder.append(rhtVal + "\n");
         // builder.append("lo: " + MiniUtils.dumpArrayList(liveout) + "\n");
      }

      return builder.toString();
   }

   private String[] sourceReg = new String[2];
   private String destinationReg = null;

   public String[] getSourceReg() {
      return sourceReg;
   }

   public String getDestinationReg() {
      return destinationReg;
   }

   private void setReg() {
      sourceReg[0] = setRegPossibleOffset(lftVal);
      sourceReg[1] = setRegPossibleOffset(rhtVal);

      destinationReg = setRegH(rhtVal);
   }

   private String setRegPossibleOffset(String str) {
      if(str != null && str.matches("[0-9]+[(][a-z,0-9,A-Z]*r[0-9]+[)]")) {
         String[] sString = str.split("[()]");
         return sString[1];
      }

      return setRegH(str);
   }

   private String setRegH(String str) {
      if(str != null && (str.matches("[a-z,0-9,A-Z]*r[0-9]+") || str.matches("%r[a-z,0-9]+"))) {
         return str;
      }

      return null;
   }

   public void replaceInstructionsX86(HashMap<String,String> coloredNodes, ArrayList<InstructionX86> instList) {
      lftVal = replacer(lftVal, coloredNodes, instList);
      midVal = replacer(midVal, coloredNodes, null);
      rhtVal = replacer(rhtVal, coloredNodes, null);

      // both left and right are memory
      if(lftVal != null && rhtVal != null &&
         (lftVal.contains("(") || lftVal.contains(MiniUtils.READ_LOC)) && rhtVal.contains("(")) {
         //helperInst =
         if(op == X86Op.CMP || op == X86Op.OR || op == X86Op.AND) {
            instList.add(instList.indexOf(this) + 1, new InstructionX86(op, MiniUtils.H_REG.getStr(), rhtVal));
            op = X86Op.MOVQ;
         } else if(op == X86Op.ADDQ || op == X86Op.SUBQ) {
            instList.add(instList.indexOf(this), new InstructionX86(X86Op.MOVQ, rhtVal, MiniUtils.H_REG.getStr()));
            instList.add(instList.indexOf(this) + 1, new InstructionX86(X86Op.MOVQ, MiniUtils.H_REG.getStr(), rhtVal));
         } else {
            instList.add(instList.indexOf(this) + 1, new InstructionX86(X86Op.MOVQ, MiniUtils.H_REG.getStr(), rhtVal));
         }
         rhtVal = MiniUtils.H_REG.getStr();
      }

      if((op == X86Op.IMULQ || op == X86Op.CMOVE || op == X86Op.CMOVLE ||
         op == X86Op.CMOVGE || op == X86Op.CMOVL || op == X86Op.CMOVG ||
         op == X86Op.CMOVNE || op == X86Op.NOT)) {
         if(rhtVal.contains("(")) {
            String offset = rhtVal.split("[(]")[0];
            if(Integer.parseInt(offset) > 16 || op == X86Op.IMULQ) { // imulq can't have memory access as second operand
               instList.add(instList.indexOf(this) + 1, new InstructionX86(X86Op.MOVQ, MiniUtils.H_REG.getStr(), rhtVal));
               instList.add(instList.indexOf(this), new InstructionX86(X86Op.MOVQ, rhtVal, MiniUtils.H_REG.getStr()));
               rhtVal = MiniUtils.H_REG.getStr();
            }
         }
      }
   }

   private String replacer(String val, HashMap<String,String> coloredNodes, ArrayList<InstructionX86> instList) {
      if(val != null && val.matches("[a-z,0-9,A-Z]*r[0-9]+")) {
         if(coloredNodes.containsKey(val)) {
            return coloredNodes.get(val);
         } else {
            return MiniUtils.H_REG.getStr();
         }
      } else if(val != null && val.matches("[0-9]+[(][a-z,0-9,A-Z]*r[0-9]+[)]")){
         String[] sString = val.split("[()]");
         if(coloredNodes.containsKey(sString[1])) {
            String newReg = coloredNodes.get(sString[1]);
            if(newReg.contains("(")) {
               // double memory access
               return doubleMemoryAccessReplacer(sString[0], newReg, instList);
            } else {
               return sString[0] + "(" + newReg + ")";
            }
         } else {
            return MiniUtils.H_REG.getStr();
         }
      }

      return val;
   }

   private String doubleMemoryAccessReplacer(String offset, String newReg, ArrayList<InstructionX86> instList) {
      if(instList != null) {
         int curIndex = instList.indexOf(this);

         instList.add(curIndex++, new InstructionX86(X86Op.MOVQ, newReg, MiniUtils.H_REG.getStr()));

         // hack/special case
         // if(op == X86Op.LEA) {
         //    op = X86Op.MOVQ;
         // }

         return offset + "(" + MiniUtils.H_REG.getStr() + ")";
      }

      return newReg;
   }
}
