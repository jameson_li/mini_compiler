public enum X86Op {
   MOVQ ("movq"),

   CALL ("call"),
   RET  ("ret"),
   LEAVE("leave"),

   LEA  ("lea"),

   OR   ("or"),
   AND  ("and"),
   NOT  ("not"),

   CMP  ("cmp"),
   CMPQ ("cmpq"),

   JE   ("je"),
   JMP  ("jmp"),
   JGE  ("jge"),
   JG   ("jg"),
   JLE  ("jle"),
   JL   ("jl"),
   JNE  ("jne"),

   ADDQ  ("addq"),
   SUBQ  ("subq"),
   IMULQ ("imulq"),
   IDIVQ ("idivq"),

   SARQ  ("sarq"),

   PUSHQ ("pushq"),
   POPQ  ("popq"),

   CMOVE ("cmove"),
   CMOVLE ("cmovle"),
   CMOVGE ("cmovge"),
   CMOVL ("cmovl"),
   CMOVG ("cmovg"),
   CMOVNE ("cmovne")

   ;

   public final String regStr;

   X86Op(String str) {
      this.regStr = str;
   }

   public String getStr() {
      return regStr;
   }
}
