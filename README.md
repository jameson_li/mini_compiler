# Mini Compiler

CSC 431 - Programming Languages II

Jameson Li 

## Getting Started

Unzip the source file. Change to the source directory.
* example: 'unzip mini_compiler.zip'

## Compile 

Use the command 'make' to compile the source.

## Usage

Option 1: Compiles source, runs compiler against all benchmarks, and compares all program outputs with the expected output.
* 'python runtests.py'

Option 2: Compiles source, run compiler against a single benchmark, and compares program output with expected output.
* 'python runtests.py [benchmarkName]'
  * example: python runtests.py Fibonacci

Option 3: Run compiler with a mini file
* 'java Mini [options] [path to mini file]'
  * example: java Mini -s -r -dumpIL tests/benchmarks/mixed/mixed.mini 

## Compiler Options

* -s
  * generates x86_64 instructions
* -r
  * replaces virtual register with x86_64 registers
* -dumpIL
  * generates ILOC instructions
* -fi 
  * optimize instructions with function inlining
* -cp
  * optimize instructions with copy propagation
* -ucr
  * optimize instructions with useless code removal

### Compiler With Custom Options

When using the runtests.py script, you can change compiler options by editing compiler_args.py.

## Notes

* The compiler and all 20 unoptimized benchmarks are expected to work properly on unix 11-14
* All dependencies are in the lib subdirectory

